import React, { useEffect, useContext, useState } from 'react';
import userContext from "./../context/userContext"
import './../static/css/screenHome.css';
import './../static/css/template.css';
import { useParams } from "react-router-dom";
import { useHistory } from "react-router-dom";

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import DialogMsg from './shared/dialogMsg'

const axios = require('axios');

function ValidarRegistro(props) {
    let history = useHistory();
    const [msg, setMsg] = useState("");
    const [open, setOpen] = useState(false);
    let { token } = useParams();

    const handleClickOpen = () => {
        setOpen(true);
      };
    
    const handleClose = () => {
        setOpen(false);
        history.push("/")
    };
    // De forma similar a componentDidMount y componentDidUpdate
    useEffect(() => {
        const url = "http://127.0.0.1:8000/cursos/api/validar_registro/"+token
        axios.get(url).then(res => {
            const data1 = res.data;
            console.log("token", data1?.msg)
            if (data1?.msg == true){
                setMsg("Cuenta activada")
            }else{
                setMsg("la cuenta no ha sido activada url invalida")
            }
            handleClickOpen()
        }).catch(function (error) {
            console.log(error.response);
            console.log("error.request.responseText", typeof error.request.responseText, error.request.responseText);
            console.log(error.message);
            let response404json = JSON.parse(error.request.responseText)
            setMsg("la cuenta no ha sido activada url invalida")
            handleClickOpen()
          });
    },[]);
    return (
        <>
            <DialogMsg open={open} handleClose={handleClose} ttitle="Mensaje" msg={msg}/>
            {/*
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{"MSG"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {msg}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                <Button onClick={handleClose} color="primary" autoFocus>
                    Cerrar
                </Button>
                </DialogActions>
            </Dialog>
            */}
        </>
    )
}
export default ValidarRegistro