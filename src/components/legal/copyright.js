import React, { useEffect, useContext, useState } from 'react';
import './../../static/css/template.css';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';


function Copyright(props) {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
          {'Copyright © '}
          <Link color="inherit" href="/">
            Dialogo Diabetes Care
          </Link>{' '}
          {new Date().getFullYear()}
          {'.'}
        </Typography>
      );
}
export default Copyright