import React, { useEffect, useContext, useState, useRef } from 'react';
import userContext from "../../context/userContext"
import './../../static/css/screenHome.css';
import './../../static/css/template.css';
import EditarCrearCuestionario from '../shared/modulos/editarCrearCuestionario'
import EditarCrearDiapositiva from '../shared/modulos/editarCrearDiapositiva'
import axios from 'axios';
import ContainerTableCrud from './containerTableCrud'
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import { TrendingUpTwoTone } from '@material-ui/icons';

import BtnAddGroup from './../shared/btnAddGroup';
import BtnAddUser from './../shared/btnAddUser';
import AddGroup from './../shared/addGroup';
import AddUser from './../shared/addUser';
import DialogMsg from "./../shared/dialogMsg"
import asyncLocalStorage from '../../utils/asyncLocalStorage'

import BtnView from './../shared/btnVIew';
import View from './../shared/modulos/view';



//http://127.0.0.1:8000/cursos/api/Diplomado/
//http://127.0.0.1:8000/cursos/api/Curso/
//http://127.0.0.1:8000/cursos/api/Modulo/

const data_ContainerTableCrud = {
    url_base : "http://127.0.0.1:8000/cursos/api/Modulo/",
    nombreVista : "modulo",
    tituloH : "Modulos"
}
function ScreenModulos(props) {
    const [context, setContext] = useContext(userContext);
    const [tipoForm, setTipoForm] = useState();
    const childRefContainer = useRef();
    const [editCurrent, setEditCurrent] = useState();
    const [data, setData] = useState([]);
    const [openSesion, setOpenSession] = useState(false);
    const [permisions, setPermisions] = useState({
        add: false,
        change: false,
        delete: false,
    });
    const handleCloseSesion = () => {
      asyncLocalStorage.clear().then(function () {
          window.location.href = '/'
      })
    };
    const [modalStatusView, setModalStatusView]=useState(false);
    const abrirCerrarModalView=()=>{
      console.log("**********************************************abrirCerrarModalView")
      setModalStatusView(!modalStatusView);
    }
    const [modalStatusEditarUser, setModalStatusEditarUser]=useState(false);
    const [modalStatusEditarGroup, setModalStatusEditarGroup]=useState(false);
    const abrirCerrarModalUser=()=>{
      console.log("**********************************************abrirCerrarModalUser")
      setModalStatusEditarUser(!modalStatusEditarUser);
    }
    const abrirCerrarModalGroup=()=>{
      console.log("**********************************************abrirCerrarModalGroup")
      setModalStatusEditarGroup(!modalStatusEditarGroup);
    }
    let tableFieldInit = [
      //{ id: 'Agregar Usuario', label: 'Agregar Usuario', minWidth: 5, type: <BtnAddUser handleClickOpenModal={abrirCerrarModalUser} setRowSelected={setEditCurrent}/> },
      //{ id: 'Agregar Grupo', label: 'Agregar Grupo', minWidth: 5, type: <BtnAddGroup handleClickOpenModal={abrirCerrarModalGroup} setRowSelected={setEditCurrent}/> },
      { id: 'id', label: 'id', minWidth: 10 },
      { id: 'nombre',label: 'Nombre', minWidth: 70, align: 'left'},
      //{ id: 'tipo',label: 'tipo', minWidth: 70, align: 'left'},
      //{ id: 'estado',label: 'estado',minWidth: 170,align: 'right',format: (value) => value.toLocaleString('en-US'),},
      //{ id: 'direccion',label: 'direccion',minWidth: 170,align: 'right',format: (value) => value.toFixed(2),},
    ]
    for(let row of context?.groups[0].permisosview ){
      let nombre = row.obj_permisos_view.obj_permisos_option_view.nombre
      console.log("nombre", nombre)
      if(nombre==data_ContainerTableCrud.nombreVista ){
          let change = {
              add_user: false,
              add_group: false
          }
          if(row.obj_permisos_view.detalle2 === true){
            tableFieldInit.push({ id: 'Ver', label: 'Ver', minWidth: 5, type: <BtnView handleClickOpenModal={abrirCerrarModalView} setRowSelected={setEditCurrent}/> });
          }
          if(row.obj_permisos_view.add_user === true){
              change.add_user = true
              tableFieldInit.push({ id: 'Agregar Usuario', label: 'Agregar Usuario', minWidth: 5, type: <BtnAddUser handleClickOpenModal={abrirCerrarModalUser} setRowSelected={setEditCurrent}/> });
          }
          if(row.obj_permisos_view.add_group === true){
              change.add_group = true
              tableFieldInit.push({ id: 'Agregar Grupo', label: 'Agregar Grupo', minWidth: 5, type: <BtnAddGroup handleClickOpenModal={abrirCerrarModalGroup} setRowSelected={setEditCurrent}/> });
          }
          //console.log("add_TableStyle 1", add_TableStyle)
          //console.log("add_TableStyle 1",[...add_TableStyle,...tableStyle])
          ////console.log("add_TableStyle 1", {...permisions, ...change })
          //setTableStyle(
          //    [...add_TableStyle,...tableStyle]
          //)
          //setPermisions({...permisions, ...change })
      }
    }
    const [tableStyle, setTableStyle] = useState(tableFieldInit);
    useEffect(() => {

    },[]);
    //const [modalEditarCuestionario, setModalEditarCuestionario]=useState(false);

    const [modalEditarDiapositiva, setModalEditarDiapositiva]=useState(false);
    //const abrirCerrarModalEditarCrearCuestionario=(tipo=1)=>{
    //    if(tipo==1){
    //        setTipoForm("Editar Cuestionario")
    //    }else{
    //        setTipoForm("Agregar Cuestionario")
    //    }
    //    setModalEditarCuestionario(!modalEditarCuestionario);
    //}
    const abrirCerrarModalEditarCrearDiapositiva=(tipo=1)=>{
        console.log("**********************************************abrirCerrarModalEditarCrearDiapositiva")
        if(tipo==1){
            setTipoForm("Editar Diapositiva")
        }else{
            setTipoForm("Agregar Diapositiva")
        }
        setModalEditarDiapositiva(!modalEditarDiapositiva);
    }
    const abrirCerrarModalEditarCrear=(tipo=1, editCurrent)=>{
      console.log("**********************************************abrirCerrarModalEditarCrear")
      console.log("tipo", tipo)
      console.log("editCurrent", editCurrent)
      setEditCurrent(editCurrent)
      //if(editCurrent.tipo=="cuestionario"){
      //  abrirCerrarModalEditarCrearCuestionario(tipo)
      //}else
      if(editCurrent.tipo=="diapositiva"){
        abrirCerrarModalEditarCrearDiapositiva(tipo)
      }
    }
    const add_custome = (
      <>
        <Grid container spacing={3}>
          {/*
          <Grid item xs={6}>
            <Button
                onClick={abrirCerrarModalEditarCrearCuestionario}
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                //className={classes.submit}
            >
                Agregar Cuestionario
            </Button>
          </Grid>
          */}
          <Grid item xs={12}>
            <Button
                onClick={abrirCerrarModalEditarCrearDiapositiva}
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                //className={classes.submit}
            >
                Agregar Diapositiva
            </Button>
          </Grid>
        </Grid>
      </>
    )
    const deleteRegistryParent = (rows, rowSelected) => {
        axios.delete(data_ContainerTableCrud.url_base+rowSelected+"/", { headers: { Authorization: 'Token ' + context.token } }).then(response => {
          console.log("response.data", response.data)
          console.log("rowSelected", rowSelected)
          console.log(typeof(rows))
          console.log(rows)
          let rows_copy = rows.filter(row => row["id"] != rowSelected)
          console.log(rows_copy)
          setData(rows_copy)
      })
      .catch(error => {
          // Capturamos los errores
          console.log("error.request.responseText", typeof error.request?.responseText);
          console.log(error.message);
          if(error.response.status==401){
            setOpenSession(true)
          }
          //else if(error.response.status==404){
          //  let rows_copy = rows.filter(row => row["id"] != rowSelected)
          //  console.log(rows_copy)
          //  setData(rows_copy)
          //}
      })
    }
    const getRegistry = () => {
      console.log("===================================getRegistry")
      let data_store = []
      return axios.get(data_ContainerTableCrud.url_base, { headers: { Authorization: 'Token ' + context.token } }).then(response => {
          /*
            "id": 36,
            "nombre": "nombre 1",
            "obj_diapositiva": null,
            "obj_discurso": null,
            "obj_curso": null,
            "created_at": "28-08-2021 03:27:16",
            "updated_at": "28-08-2021 03:47:36"
          */
          console.log("response.data :)", response.data)
          for(let row of response.data){
            console.log(row)
            let local = {}
            local["id"] = row.id
            local["nombre"] = row.nombre
            local["obj_diapositiva"] = row.obj_diapositiva
            if( row.obj_diapositiva!=null ){
              local["tipo"] = "diapositiva"
            }else if( row.obj_discurso!=null ){
              local["tipo"] = "discurso"
            }
            data_store.push(local)
          }
          console.log("data_store", data_store)
          setData(data_store)
      }).catch(error => {
        if(error.response?.status==401){
          //childRefContainer.current.handleClickOpenSesion()
          setOpenSession(true)
        }
        // Capturamos los errores
      })
    }
    let bodyEdit = (<>
      {/*
      <EditarCrearCuestionario
        modalEditar={modalEditarCuestionario}
        abrirCerrarModalEditarCrear={abrirCerrarModalEditarCrearCuestionario}
        data_ContainerTableCrud={data_ContainerTableCrud}
        permisions={permisions}
        tipoForm={tipoForm}
        setTipoForm={setTipoForm}
        getRegistry={getRegistry}
        editCurrent={editCurrent}
        />
      */}
      <EditarCrearDiapositiva
        modalEditar={modalEditarDiapositiva}
        abrirCerrarModalEditarCrear={abrirCerrarModalEditarCrearDiapositiva}
        data_ContainerTableCrud={data_ContainerTableCrud}
        permisions={permisions}
        tipoForm={tipoForm}
        setTipoForm={setTipoForm}
        getRegistry={getRegistry}
        editCurrent={editCurrent}
        openSesion={openSesion}
        setOpenSession={setOpenSession}
        />
        
    </>)
    return (
        <>
            <ContainerTableCrud {...data_ContainerTableCrud}
              tableStyle={tableStyle}
              setTableStyle={setTableStyle}
              bodyEdit={bodyEdit}
              ref={childRefContainer}
              deleteRegistryParent={deleteRegistryParent}
              getRegistry={getRegistry}
              data={data}
              setData={setData}
              abrirCerrarModalEditarCrear={abrirCerrarModalEditarCrear}
              //modalEditar={modalEditar}
              permisions={permisions}
              setPermisions={setPermisions}
              add_custome={add_custome}>
            </ContainerTableCrud>
            <AddGroup
              modalStatus={modalStatusEditarGroup}
              abrirCerrarModalEditarCrear={abrirCerrarModalGroup}
              permisions={permisions}
              editCurrent={editCurrent}
              type="obj_modulo"
            >
            </AddGroup>
            <AddUser
              modalStatus={modalStatusEditarUser}
              abrirCerrarModalEditarCrear={abrirCerrarModalUser}
              permisions={permisions}
              editCurrent={editCurrent}
              type="obj_modulo"
            >
            </AddUser>
            <View
              openSesion={openSesion}
              modalStatus={modalStatusView}
              abrirCerrarModalEditarCrear={abrirCerrarModalView}
              permisions={permisions}
              editCurrent={editCurrent}
              type="obj_modulo"
            >
            </View>
            <DialogMsg open={openSesion} handleClose={handleCloseSesion} ttitle="Importante" msg="Sesión caducada"/>
        </>
    )
}
export default ScreenModulos