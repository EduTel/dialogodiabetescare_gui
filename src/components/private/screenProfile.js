import React, { useEffect, useContext, useState } from 'react';
import userContext from "../../context/userContext"
import './../../static/css/screenHome.css';
import './../../static/css/template.css';

import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import Copyright from '../legal/copyright'
import asyncLocalStorage from '../../utils/asyncLocalStorage'


const useStyles = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(3),
    }
  }));
function ScreenProfile(props) {
    const classes = useStyles();
    const [values, setValues] = useState({});

    useEffect(() => {
        console.log("*******************useEffect /Home/profile")
        asyncLocalStorage.getItem("dialogodiabetescare").then(function (res) {
            res = JSON.parse(res)
            setValues({...values, ...res});
        })
      },[]);
    return(
        <>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <div className={classes.paper}>
                <form className={classes.form} noValidate>
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                autoComplete="fname"
                                name="first_name"
                                variant="outlined"
                                required
                                fullWidth
                                id="first_name"
                                label="Nombre"
                                value={values.first_name}
                                disabled
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                id="last_name"
                                label="Apellidos"
                                name="last_name"
                                autoComplete="last_name"
                                value={values.last_name}
                                disabled
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                id="especialidad"
                                label="Especialidad"
                                name="especialidad"
                                autoComplete="especialidad"
                                value={values.especialidad}
                                disabled
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                autoComplete="fname"
                                name="ciudad"
                                variant="outlined"
                                required
                                fullWidth
                                id="ciudad"
                                label="Ciudad"
                                value={values.ciudad}
                                disabled
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                id="estado"
                                label="Estado"
                                name="estado"
                                autoComplete="estado"
                                value={values.estado}
                                disabled
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                name="direccion"
                                label="Dirección"
                                type="direccion"
                                id="direccion"
                                autoComplete="direccion"
                                value={values.direccion}
                                disabled
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                autoComplete="fname"
                                name="telefono"
                                variant="outlined"
                                required
                                fullWidth
                                id="telefono"
                                label="Teléfono"
                                value={values.telefono}
                                disabled
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                id="celular"
                                label="Celular"
                                name="celular"
                                autoComplete="celular"
                                value={values.celular}
                                disabled
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                name="email"
                                label="Correo electrónico"
                                type="email"
                                id="email"
                                autoComplete="email"
                                value={values.email}
                                disabled
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                name="cedulaProfesional"
                                label="Cédula profesional"
                                type="cedulaProfesional"
                                id="cedulaProfesional"
                                autoComplete="cedulaProfesional"
                                value={values.cedulaProfesional}
                                disabled
                            />
                        </Grid>
                    </Grid>
                </form>
            </div>
            </Container>
        </>
    )
}
export default ScreenProfile