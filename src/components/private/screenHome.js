import React, { useEffect, useContext, useState } from 'react';
import userContext from "../../context/userContext"
import './../../static/css/screenHome.css';
import './../../static/css/template.css';

function ScreenHome(props) {
    const [context, setContext] = useContext(userContext);
    const [table, setTable] = useState([]);
    return (
        <>
            <h1 className="title">welcome to you online banking {context?.name}</h1>
        </>
    )
}
export default ScreenHome