import React, { useEffect, useContext, useState, useImperativeHandle, forwardRef, handleClickOpenSesion } from 'react';
import userContext from "../../context/userContext"
import './../../static/css/screenHome.css';
import './../../static/css/template.css';
import TableCrud from './../shared/tableCrud'
import {Box} from '@material-ui/core';
import axios from 'axios';
import DialogMsg from "../shared/dialogMsg"
import asyncLocalStorage from '../../utils/asyncLocalStorage'
import { useHistory } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Fade from '@material-ui/core/Fade';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

function ContainerTableCrud(props) {
//const ContainerTableCrud = forwardRef((props, ref) => {
    const {url_base, nombreVista, tableStyle, setTableStyle, bodyEdit, tituloH, abrirCerrarModalEditarCrear, permisions, setPermisions, add_custome, deleteRegistryParent, getRegistry, data, setData} = props;
    const [context, setContext] = useContext(userContext);
    const [openSesion, setOpenSession] = useState(false);
    //const [data, setData] = useState([]);
    const handleClickOpenSesion = () => {
        setOpenSession(true);
    };
    //useImperativeHandle(ref, () => ({
    //    handleClickOpenSesion
    //}));
    const handleCloseSesion = () => {
        asyncLocalStorage.clear().then(function () {
            window.location.href = '/'
        })
    };
    useEffect(() => {
        if(getRegistry!=undefined){
            getRegistry()
        }else{
            axios.get(url_base, { headers: { Authorization: 'Token ' + context.token } }).then(response => {
                console.log("response.data", response.data)
                setData(response.data)
            })
            .catch(error => {
                if(error.response.status==401){
                    handleClickOpenSesion()
                }
                // Capturamos los errores
            })
        }
    },[]);
    useEffect(() => {
        context?.groups[0].permisosview.map(row => {
            let nombre = row.obj_permisos_view.obj_permisos_option_view.nombre
            console.log("nombreVista", nombreVista)
            if(nombre==nombreVista ){
                let change = {
                    add: false,
                    change: false,
                    delete: false,
                }
                let add_TableStyle = []
                if (row.obj_permisos_view.add == true){
                    console.log("***********************************add")
                    change.add = true
                }
                if(row.obj_permisos_view.delete == true){
                    change.delete = true
                    add_TableStyle.push({ id: 'eliminar',label: 'Eliminar',minWidth: 70, align: 'left'});
                }
                if(row.obj_permisos_view.detalle1 == true){
                    change.delete = true
                    add_TableStyle.push({ id: 'detalle', label: 'detalle', minWidth: 10 });
                }
                if(row.obj_permisos_view.change == true){
                    change.change = true
                }
                console.log("add_TableStyle 2", add_TableStyle)
                console.log("add_TableStyle 2",[...add_TableStyle,...tableStyle])
                console.log("add_TableStyle 2", {...permisions, ...change })
                setTableStyle(
                    [...add_TableStyle,...tableStyle]
                )
                setPermisions({...permisions, ...change })
            }
        })
    },[context]);
    return (
        <>
            <Box pt={5}>
                <Grid container spacing={3}>
                    <Grid item xs={6}>
                        <h3>{tituloH}</h3>
                    </Grid>
                    <Grid item xs={6}>
                        {permisions?.add && add_custome==undefined &&<Button
                                                        onClick={abrirCerrarModalEditarCrear}
                                                        type="submit"
                                                        fullWidth
                                                        variant="contained"
                                                        color="primary"
                                                        //className={classes.submit}
                                                    >
                                                        Agregar
                                                    </Button>
                                                    }
                        {permisions?.add && add_custome!=undefined && add_custome}
                    </Grid>
                </Grid>
                <TableCrud
                    handleClickOpenSesion={handleClickOpenSesion}
                    columns={tableStyle}
                    rows={data}
                    setData={setData}
                    url_base={url_base}
                    abrirCerrarModalEditarCrear={abrirCerrarModalEditarCrear}
                    deleteRegistryParent={deleteRegistryParent}/>
                <DialogMsg open={openSesion} handleClose={handleCloseSesion} ttitle="Importante" msg="Sesión caducada"/>
                {bodyEdit}
            </Box>
        </>
    )
}
//)
export default ContainerTableCrud