import React, { useEffect, useContext, useState } from 'react';
import userContext from "../../context/userContext"
import './../../static/css/screenHome.css';
import './../../static/css/template.css';
import EditarCrearCurses from '../shared/curses/editarCrearCurses'

import axios from 'axios';
import ContainerTableCrud from './containerTableCrud'
//edit
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import asyncLocalStorage from '../../utils/asyncLocalStorage'
import DialogMsg from "./../shared/dialogMsg"

import BtnAddGroup from './../shared/btnAddGroup';
import BtnAddUser from './../shared/btnAddUser';
import AddGroup from './../shared/addGroup';
import AddUser from './../shared/addUser';

const data_ContainerTableCrud = {
    url_base : "http://127.0.0.1:8000/cursos/api/Curso/",
    nombreVista : "cursos",
    tituloH : "Cursos"
}

function ScreenCourses(props) {
    const [context, setContext] = useContext(userContext);
    const [tipoForm, setTipoForm] = useState("");
    const [data, setData] = useState([]);
    const [openSesion, setOpenSession] = useState(false);
    const [permisions, setPermisions] = useState({
        add: false,
        change: false,
        delete: false,
    });
    const [editCurrent, setEditCurrent] = useState();
    const [modalStatusEditarUser, setModalStatusEditarUser]=useState(false);
    const [modalStatusEditarGroup, setModalStatusEditarGroup]=useState(false);
    const abrirCerrarModalUser=()=>{
      console.log("**********************************************abrirCerrarModalUser")
      setModalStatusEditarUser(!modalStatusEditarUser);
    }
    const abrirCerrarModalGroup=()=>{
      console.log("**********************************************abrirCerrarModalGroup")
      setModalStatusEditarGroup(!modalStatusEditarGroup);
    }
    let tableFieldInit = [
      //{ id: 'detalle', label: 'detalle', minWidth: 10 },
      //{ id: 'Agregar Usuario', label: 'Agregar Usuario', minWidth: 5, type: <BtnAddUser handleClickOpenModal={abrirCerrarModalUser} setRowSelected={setEditCurrent}/> },
      //{ id: 'Agregar Grupo', label: 'Agregar Grupo', minWidth: 5, type: <BtnAddGroup handleClickOpenModal={abrirCerrarModalGroup} setRowSelected={setEditCurrent}/> },
      { id: 'id', label: 'id', minWidth: 10 },
      { id: 'nombre',label: 'Nombre',minWidth: 70, align: 'left'},
      //{ id: 'estado',label: 'estado',minWidth: 170,align: 'right',format: (value) => value.toLocaleString('en-US'),},
      //{ id: 'direccion',label: 'direccion',minWidth: 170,align: 'right',format: (value) => value.toFixed(2),},
    ]
    for(let row of context?.groups[0].permisosview ){
      let nombre = row.obj_permisos_view.obj_permisos_option_view.nombre
      console.log("nombre", nombre)
      if(nombre==data_ContainerTableCrud.nombreVista ){
          let change = {
              add_user: false,
              add_group: false
          }
          //let add_TableStyle = []
          if(row.obj_permisos_view.add_user == true){
              change.add_user = true
              tableFieldInit.push({ id: 'Agregar Usuario', label: 'Agregar Usuario', minWidth: 5, type: <BtnAddUser handleClickOpenModal={abrirCerrarModalUser} setRowSelected={setEditCurrent}/> });
          }
          if(row.obj_permisos_view.add_group == true){
              change.add_group = true
              tableFieldInit.push({ id: 'Agregar Grupo', label: 'Agregar Grupo', minWidth: 5, type: <BtnAddGroup handleClickOpenModal={abrirCerrarModalGroup} setRowSelected={setEditCurrent}/> });
          }
          //console.log("add_TableStyle 1", add_TableStyle)
          //console.log("add_TableStyle 1",[...add_TableStyle,...tableStyle])
          ////console.log("add_TableStyle 1", {...permisions, ...change })
          //setTableStyle(
          //    [...add_TableStyle,...tableStyle]
          //)
          //setPermisions({...permisions, ...change })
      }
    }
    const [tableStyle, setTableStyle] = useState(tableFieldInit);
    const [modalStatus, setModalStatus]=useState(false);
    const abrirCerrarModalEditarCrear=(tipo=1, editCurrent)=>{
        console.log("**********************************************abrirCerrarModalEditarCrear")
        console.log("tipo", tipo)
        console.log("editCurrent", editCurrent)
        setEditCurrent(editCurrent)
        if(tipo==1){
            setTipoForm("Editar Cursos")
        }else{
            setTipoForm("Agregar Cursos")
        }
        setModalStatus(!modalStatus);
    }
    const handleCloseSesion = () => {
      asyncLocalStorage.clear().then(function () {
          window.location.href = '/'
      })
    };
    const getRegistry = () => {
      console.log("===================================getRegistry")
      return axios.get(data_ContainerTableCrud.url_base, { headers: { Authorization: 'Token ' + context.token } }).then(response => {
          console.log("response.data :)", response.data)
          setData(response.data)
      }).catch(error => {
        if(error.response?.status==401){
          console.log("Error al cargar")
          setOpenSession(true)
        }
      })
    }
    let bodyEdit = (<>
      <EditarCrearCurses
        modalStatus={modalStatus}
        abrirCerrarModalEditarCrear={abrirCerrarModalEditarCrear}
        //data_ContainerTableCrud={data_ContainerTableCrud}
        permisions={permisions}
        tipoForm={tipoForm}
        setTipoForm={setTipoForm}
        getRegistry={getRegistry}
        editCurrent={editCurrent}
        openSesion={openSesion}
        setOpenSession={setOpenSession}
        />
    </>)
    return (
      <>
        <ContainerTableCrud {...data_ContainerTableCrud}
          tableStyle={tableStyle}
          setTableStyle={setTableStyle}
          bodyEdit={bodyEdit}
          /*ref={childRefContainer}*/
          /*deleteRegistryParent={deleteRegistryParent}*/
          getRegistry={getRegistry}
          data={data}
          setData={setData}
          abrirCerrarModalEditarCrear={abrirCerrarModalEditarCrear}
          //modalEditar={modalEditar}
          permisions={permisions}
          setPermisions={setPermisions}
          /*add_custome={add_custome}*/
          >
        </ContainerTableCrud>
        <AddGroup
          modalStatus={modalStatusEditarGroup}
          abrirCerrarModalEditarCrear={abrirCerrarModalGroup}
          permisions={permisions}
          editCurrent={editCurrent}
          type="obj_curso"
        >
        </AddGroup>
        <AddUser
          modalStatus={modalStatusEditarUser}
          abrirCerrarModalEditarCrear={abrirCerrarModalUser}
          permisions={permisions}
          editCurrent={editCurrent}
          type="obj_curso"
        >
        </AddUser>
        <DialogMsg open={openSesion} handleClose={handleCloseSesion} ttitle="Importante" msg="Sesión caducada"/>
      </>
    )
}
export default ScreenCourses