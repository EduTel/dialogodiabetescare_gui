import React, { useEffect, useContext, useState } from 'react';
import userContext from "../../../context/userContext"
import './../../../static/css/screenHome.css';
import './../../../static/css/template.css';
import AddUser from './addUser'

import axios from 'axios';
import ContainerTableCrud from './../containerTableCrud'
//edit
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import asyncLocalStorage from '../../../utils/asyncLocalStorage'
import DialogMsg from "./../../shared/dialogMsg"

const data_ContainerTableCrud = {
  url_base : "http://127.0.0.1:8000/cursos/api/GruposInterno/",
  nombreVista : "grupos_interno",
  tituloH : "Grupos Interno"
}

function ScreenInternalGroups(props) {
    const [context, setContext] = useContext(userContext);
    const [tipoForm, setTipoForm] = useState("");
    const [data, setData] = useState([]);
    const [openSesion, setOpenSession] = useState(false);
    const [permisions, setPermisions] = useState({
        add: false,
        change: false,
        delete: false,
    });
    const [editCurrent, setEditCurrent] = useState();
    const [tableStyle, setTableStyle] = useState([
      //{ id: 'detalle', label: 'detalle', minWidth: 10 },
      { id: 'id', label: 'id', minWidth: 10 },
      { id: 'nombre',label: 'Nombre', minWidth: 70, align: 'left'}
      //{ id: 'estado',label: 'estado',minWidth: 170,align: 'right',format: (value) => value.toLocaleString('en-US'),},
      //{ id: 'direccion',label: 'direccion',minWidth: 170,align: 'right',format: (value) => value.toFixed(2),},
    ]);
    const [modalStatus, setModalStatus]=useState(false);
    const abrirCerrarModalEditarCrear=(tipo=1, editCurrent)=>{
        console.log("**********************************************abrirCerrarModalEditarCrear")
        console.log("tipo", tipo)
        console.log("editCurrent", editCurrent)
        setEditCurrent(editCurrent)
        if(tipo==1){
            setTipoForm("Editar Grupo")
        }else{
            setTipoForm("Agregar Grupo")
        }
        setModalStatus(!modalStatus);
    }
    const handleCloseSesion = () => {
      asyncLocalStorage.clear().then(function () {
          window.location.href = '/'
      })
    };
    const getRegistry = () => {
      console.log("===================================getRegistry")
      return axios.get(data_ContainerTableCrud.url_base, { headers: { Authorization: 'Token ' + context.token } }).then(response => {
          console.log("response.data :)", response.data)
          setData(response.data)
      }).catch(error => {
        if(error.response?.status==401){
          console.log("Error al cargar")
          setOpenSession(true)
        }
      })
    }
    let bodyEdit = (<>
      <AddUser
        modalStatus={modalStatus}
        abrirCerrarModalEditarCrear={abrirCerrarModalEditarCrear}
        //data_ContainerTableCrud={data_ContainerTableCrud}
        permisions={permisions}
        tipoForm={tipoForm}
        setTipoForm={setTipoForm}
        getRegistry={getRegistry}
        editCurrent={editCurrent}
        openSesion={openSesion}
        setOpenSession={setOpenSession}
        />
    </>)
    return (
      <>
        <ContainerTableCrud {...data_ContainerTableCrud}
          tableStyle={tableStyle}
          setTableStyle={setTableStyle}
          bodyEdit={bodyEdit}
          /*ref={childRefContainer}*/
          /*deleteRegistryParent={deleteRegistryParent}*/
          getRegistry={getRegistry}
          data={data}
          setData={setData}
          abrirCerrarModalEditarCrear={abrirCerrarModalEditarCrear}
          //modalEditar={modalEditar}
          permisions={permisions}
          setPermisions={setPermisions}
          /*add_custome={add_custome}*/
          >
        </ContainerTableCrud>
        <DialogMsg open={openSesion} handleClose={handleCloseSesion} ttitle="Importante" msg="Sesión caducada"/>
      </>
    )
}

export default ScreenInternalGroups