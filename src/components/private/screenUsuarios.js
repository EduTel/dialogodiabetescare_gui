import React, { useEffect, useContext, useState } from 'react';
import userContext from "../../context/userContext"
import './../../static/css/screenHome.css';
import './../../static/css/template.css';
import axios from 'axios';
import ContainerTableCrud from './containerTableCrud'
//edit
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';

const data_ContainerTableCrud = {
    url_base : "http://127.0.0.1:8000/cursos/api/UserCustom/",
    nombreVista : "usercustom",
    tituloH : "Usuarios"
}

const useStyles = makeStyles((theme) => ({
    paper: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
    modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
}));

function ScreenUsuarios(props) {
    const initialFormValues = {
        username: "",
        password: "",
    }
    const [values, setValues] = useState(initialFormValues);
    const [errorsForm, setErrorsForm] = useState({});
    const [context, setContext] = useContext(userContext);
    const [errors, setErrors] = useState(false);
    const [errorsMsg, setErrorsMsg] = useState("");
    const classes = useStyles();
    const [tipoForm, setTipoForm] = useState();
    const [data, setData] = useState([]);
    const [permisions, setPermisions] = useState({
        add: false,
        change: false,
        delete: false,
    });
    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setValues({
          ...values,
          [name]: value
        });
        validate({ [name]: value });
      };
    const [tableStyle, setTableStyle] = useState([
        { id: 'detalle', label: 'detalle', minWidth: 10 },
        { id: 'id', label: 'id', minWidth: 10 },
        { id: 'first_name',label: 'Nombre',minWidth: 70, align: 'left'},
        { id: 'last_name',label: 'Apellido',minWidth: 70, align: 'left'},
        { id: 'email',label: 'Correo',minWidth: 170, align: 'left'},
        { id: 'ciudad', label: 'Ciudad', minWidth: 170, align: 'left'},
        { id: 'estado',label: 'Estado',minWidth: 170, align: 'left'},
        { id: 'direccion',label: 'Dirección',minWidth: 170, align: 'left'},
        { id: 'telefono',label: 'Teléfono',minWidth: 170, align: 'left'},
        { id: 'celular',label: 'Celular',minWidth: 170, align: 'left'},
        { id: 'cedulaProfesional',label: 'Cedula Profesional',minWidth: 170, align: 'left'},
        { id: 'especialidad',label: 'Especialidad',minWidth: 170, align: 'left'},
        //{ id: 'estado',label: 'estado',minWidth: 170,align: 'left',format: (value) => value.toLocaleString('en-US'),},
        //{ id: 'direccion',label: 'direccion',minWidth: 170,align: 'left',format: (value) => value.toFixed(2),},
    ]);
    const validate = (fieldValues = values) => {
        let temp = { ...errorsForm };
        console.log("temp", temp)
    
        if ("username" in fieldValues) {
          temp.username = fieldValues.username ? "" : "El campo es requerido.";
          if (fieldValues.username)
            temp.username = /^[^@\s]+@[^@\s]+\.[^@\s]+$/.test(fieldValues.username)?"": "El Correo no es valido.";
        }
    
        if ("password" in fieldValues)
          temp.password = fieldValues.password ? "" : "El campo es requerido.";
    
        console.log("temp", temp)
        let enviar = true
        for(let row_data in temp){
          if(temp[row_data] != ""){
            enviar = false
            break
          }
        }
        setErrorsForm({
          ...temp
        });
        return enviar
    };
    const handleSubmit = async (event) => {
        event.preventDefault();
        let enviar = validate(values);
        //console.log(errorsForm)
        //let enviar = true
        //for(let row_data in errorsForm){
        //  if(errorsForm[row_data] != ""){
        //    enviar = false
        //    break
        //  }
        //}
        if( enviar ){
          //const options = {
          //  headers: {'X-Custom-Header': 'value'}
          //};
          var data_store = {}
          axios.post(data_ContainerTableCrud.url_base,values)
          .then(res => {
            data_store = res.data;
          }).catch(function (error) {
            console.log("=========================catch")
            setContext({})
            console.log(error.response);
            console.log("error.request.responseText", typeof error.request.responseText, error.request.responseText);
            console.log(error.message);
            let response400json = JSON.parse(error.request.responseText)
            if (Object.getOwnPropertyNames(response400json).includes("non_field_errors")){
              //console.log(response400json.get["password"])
              setErrors(false)
              setErrorsMsg("")
              for( let row_data of response400json?.non_field_errors){
                console.log(row_data)
                if(row_data == "Unable to log in with provided credentials."){
                  setErrors(true)
                  setErrorsMsg("Usuario o contraseña incorrecto")
                }
              }
            }
          })
        }
    }
    const [modalEditar, setModalEditar]=useState(false);
    const abrirCerrarModalEditarCrear=(tipo=1)=>{
        if(tipo==1){
            setTipoForm("Editar")
        }else{
            setTipoForm("Agregar")
        }
        setModalEditar(!modalEditar);
    }
    let bodyEdit = (<>
      <Modal
          open={modalEditar}
          className={classes.modal}
          closeAfterTransition={false}
          BackdropComponent={Backdrop}
          BackdropProps={{
              timeout: 0,
          }}
          onClose={abrirCerrarModalEditarCrear}>
              <div className={classes.paperDiv}>
                <Container component="main" maxWidth="xs">
                <CssBaseline />
                  <div className={classes.paper}>
                      <Typography component="h5" variant="h5">{tipoForm}</Typography>
                      <form className={classes.form} noValidate>
                          <TextField
                              variant="outlined"
                              margin="normal"
                              required
                              fullWidth
                              id="username"
                              label="Correo electronico"
                              name="username"
                              autoComplete="username"
                              autoFocus
                              onBlur={handleInputChange}
                              onChange={handleInputChange}
                              required
                              {...(errorsForm["username"] && {
                              error: true,
                              helperText: errorsForm["username"]
                              })}
                          />
                          <TextField
                              variant="outlined"
                              margin="normal"
                              required
                              fullWidth
                              name="password"
                              label="Contraseña"
                              type="password"
                              id="password"
                              autoComplete="current-password"
                              onBlur={handleInputChange}
                              onChange={handleInputChange}
                              required
                              {...(errorsForm["password"] && {
                              error: true,
                              helperText: errorsForm["password"]
                              })}
                          />
                          { errors &&
                              <Typography variant="h5" component="h2" style={{ color: "red" }}>
                                  {errorsMsg}
                              </Typography>
                          }
                          {
                            ( (tipoForm=="Agregar" && permisions.add) || (tipoForm=="Editar" && permisions.change) ) &&
                                  <Button
                                      onClick={handleSubmit}
                                      type="submit"
                                      fullWidth
                                      variant="contained"
                                      color="primary"
                                      className={classes.submit}
                                  >
                                  Guardar
                              </Button>
                          }
                      </form>
                  </div>
                </Container>
              </div>
      </Modal>
  </>)
    return (
        <>
            <ContainerTableCrud {...data_ContainerTableCrud} 
              tableStyle={tableStyle}
              setTableStyle={setTableStyle}
              bodyEdit={bodyEdit}
              data={data}
              setData={setData}
              abrirCerrarModalEditarCrear={abrirCerrarModalEditarCrear}
              modalEditar={modalEditar}
              permisions={permisions}
              setPermisions={setPermisions}>
            </ContainerTableCrud>
        </>
    )
}
export default ScreenUsuarios