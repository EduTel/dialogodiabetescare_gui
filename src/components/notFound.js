import React, { useEffect, useContext, useState } from 'react';
import userContext from "../context/userContext"
import './../static/css/template.css';
import { Typography } from '@material-ui/core';

function NotFound(props) {
    return (
        <>
            <Typography variant="h3" component="h2" align="center">
                Error 404 Página no encontrada
            </Typography>
        </>
    )
}
export default NotFound