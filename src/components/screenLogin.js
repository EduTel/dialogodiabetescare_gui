import React, { useState, useEffect, useMemo, useContext }  from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import userContext from "./../context/userContext"
import { useHistory } from "react-router-dom";

import { ValidatorComponent } from 'react-material-ui-form-validator';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';

import asyncLocalStorage from './../utils/asyncLocalStorage'
//import {browserHistory} from 'react-router'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

import Copyright from './legal/copyright'
const axios = require('axios');

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignIn() {
  const [context, setContext] = useContext(userContext);
  const classes = useStyles();
  let history = useHistory();

  const initialFormValues = {
    username: "",
    password: "",
  }

  const [values, setValues] = useState(initialFormValues);
  const [errorsForm, setErrorsForm] = useState({});
  const [errors, setErrors] = useState(false);
  const [errorsMsg, setErrorsMsg] = useState("");
  
  const validate = (fieldValues = values) => {
    let temp = { ...errorsForm };
    console.log("temp", temp)

    if ("username" in fieldValues) {
      temp.username = fieldValues.username ? "" : "El campo es requerido.";
      if (fieldValues.username)
        temp.username = /^[^@\s]+@[^@\s]+\.[^@\s]+$/.test(fieldValues.username)?"": "El Correo no es valido.";
    }

    if ("password" in fieldValues)
      temp.password = fieldValues.password ? "" : "El campo es requerido.";

    console.log("temp", temp)
    let enviar = true
    for(let row_data in temp){
      if(temp[row_data] != ""){
        enviar = false
        break
      }
    }
    setErrorsForm({
      ...temp
    });
    return enviar
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value
    });
    validate({ [name]: value });
  };

  const handleSubmit = async (event) => {
      event.preventDefault();
      console.log("values 2:")
      console.log(values)
      //for (let name in values){
      //  console.log({ [name]: initialFormValues[name]})
      //  validate({ [name]: initialFormValues[name]});
      //}
      validate(values);
      console.log(errorsForm)
      let enviar = true
      for(let row_data in errorsForm){
        if(errorsForm[row_data] != ""){
          enviar = false
          break
        }
      }
      
      if( enviar )
        //const options = {
        //  headers: {'X-Custom-Header': 'value'}
        //};
        var data_store = {}
        axios.post(`http://localhost:8000/cursos/api/api-token-auth`,values)
        .then(res => {
          data_store = res.data;
          console.log("token", data_store?.token)
          if (data_store?.token != undefined){
            //setContext(data_store)
            const UserCustom_url = `http://localhost:8000/cursos/api/UserCustom/${data_store?.user}`
            console.log("UserCustom_url"+UserCustom_url)
            return axios.get(UserCustom_url, { headers: { Authorization: 'Token '+data_store?.token } })
          }
        }).then(res => {
          let data2 = res.data;
          console.log("data2", data2)
          data_store = {...data_store, ...data2}
          console.log("data_store1", data_store)
          const PermisosView_url = `http://127.0.0.1:8000/cursos/api/PermisosView/`
          console.log("PermisosView_url"+PermisosView_url)
          return axios.get(PermisosView_url, { headers: { Authorization: 'Token '+data_store?.token } })
        }).then(res => {
          let data3 = res.data;
          console.log("data3", data3)
          console.log("data_store2", data_store)
          data_store.groups[0]["permisosview"] = data3
          setContext(data_store)
          console.log("context all",data_store)
          asyncLocalStorage.setItem('dialogodiabetescare', JSON.stringify(data_store) ).then(function () {
            window.location.href = '/home'
          })
          //localStorage.setItem('dialogodiabetescare', JSON.stringify(data_context) );
        }).catch(function (error) {
          console.log("=========================catch")
          setContext({})
          console.log(error.response);
          console.log("error.request.responseText", typeof error.request.responseText, error.request.responseText);
          console.log(error.message);
          let response400json = {}
          try {
            response400json = JSON.parse(error.request.responseText)
          }catch (e) {
            console.log(e)
          }
          if (Object.getOwnPropertyNames(response400json).includes("non_field_errors")){
            //console.log(response400json.get["password"])
            setErrors(false)
            setErrorsMsg("")
            for( let row_data of response400json?.non_field_errors){
              console.log(row_data)
              if(row_data == "Unable to log in with provided credentials."){
                setErrors(true)
                setErrorsMsg("Usuario o contraseña incorrecto")
              }
            }
          }

        })
        //setContext({"name": formulario.username.trim(), acount: 123456789})
        //
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="username"
            label="Correo electronico"
            name="username"
            autoComplete="username"
            autoFocus
            onBlur={handleInputChange}
            onChange={handleInputChange}
            required
            {...(errorsForm["username"] && {
              error: true,
              helperText: errorsForm["username"]
            })}
            //helperText="Incorrect entry."
            //error
            //error={text === ""}
            //helperText={text === "" ? 'Empty!' : ' '}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Contraseña"
            type="password"
            id="password"
            autoComplete="current-password"
            onBlur={handleInputChange}
            onChange={handleInputChange}
            required
            {...(errorsForm["password"] && {
              error: true,
              helperText: errorsForm["password"]
            })}
            //helperText="Incorrect entry."
            //error
            //error={text === ""}
            //helperText={text === "" ? 'Empty!' : ' '}
          />
          { errors &&
            <Typography variant="h5" component="h2" style={{ color: "red" }}>
              {errorsMsg}
            </Typography>
          }
          {/*
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
          */}
          <Button
            onClick={handleSubmit}
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Entrar
          </Button>
          <Grid container>
            <Grid item xs>
              <Link href="/OlvideMiPassword" variant="body2">
                Olvidé mi contraseña
              </Link>
            </Grid>
            <Grid item>
              <Link href="/CrearCuenta" variant="body2">
                {"¿No tienes una cuenta? crea una"}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}