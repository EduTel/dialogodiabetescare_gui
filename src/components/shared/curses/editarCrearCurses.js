import React, { useEffect, useContext, useState } from 'react';
import userContext from "./../../../context/userContext"
//edit
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import green from '@material-ui/core/colors/green';
import IconButton from '@material-ui/core/IconButton';
import RemoveIcon from '@material-ui/icons/Remove';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import Paper from '@material-ui/core/Paper';
import { v4 as uuidv4 } from 'uuid';

import axios from 'axios';

import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import { FilledInput } from '@material-ui/core';
//import RaisedButton from '@material-ui/RaisedButton';

import asyncLocalStorage from '../../../utils/asyncLocalStorage'
import DialogMsg from "./../dialogMsg"

import { Transfer } from 'antd';
import 'antd/dist/antd.css';

const useStyles = makeStyles((theme) => ({
  paperDiv: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '80%',
    maxHeight: '100%',
    overflow: 'auto',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  container: {
    width: '100%',
    padding: 0,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
  },
  paper: {
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
  },
}));
function EditarCrearCurses(props) {
    const {modalStatus, abrirCerrarModalEditarCrear, permisions, tipoForm, getRegistry, editCurrent, openSesion, setOpenSession} = props;
    const initialFormValues = {
        nombreCurso: ""
    }
    const { v4: uuidv4 } = require('uuid');
    const [data_contenido, setData_Contenido] = useState([]);
    const [context, setContext] = useContext(userContext);
    const [errors, setErrors] = useState(false);
    const [errorsMsg, setErrorsMsg] = useState("");
    const [errorsForm, setErrorsForm] = useState({});
    const [values, setValues] = useState(initialFormValues);
    //const [openSesion, setOpenSession] = useState(false);
    const classes = useStyles();
    const handleCloseSesion = () => {
      asyncLocalStorage.clear().then(function () {
          window.location.href = '/'
      })
    };
    const handleClickOpenSesion = () => {
      setOpenSession(true);
    };
    const handleInputChange = (e) => {
        console.log("__________handleInputChange")
        console.log("e", e)
        const { name, value } = e.target;
        setValues({
          ...values,
          [name]: value
        });
        validate({ [name]: value });
    }
    const validate = (fieldValues = values) => {
        console.log("=================================validate")
        console.log("fieldValues", fieldValues)
        console.log("values", values)
        let temp = { ...errorsForm };
        console.log("temp", temp)
    
        if ("iframe" in fieldValues) {
          temp.iframe = fieldValues.iframe ? "" : "El campo es requerido.";
        }
        if ("nombreModulo" in fieldValues) {
          console.log("fieldValues.nombreModulo", fieldValues.nombreModulo)
          temp.nombreModulo = fieldValues.nombreModulo ? "" : "El campo es requerido.";
        }
        
        for(let row in  fieldValues){
          if(row.startsWith('mu_')){
            console.log("row", row)
            temp[row] = fieldValues[row]  ? "" : "El campo es requerido.";
          }
        }
    
        console.log("temp", temp)
        let enviar = true
        for(let row_data in temp){
          if(temp[row_data] != ""){
            enviar = false
            break
          }
        }
        setErrorsForm({
          ...temp
        });
        console.log("=================================", enviar)
        return enviar
    };
    const handleSubmit = async (event) => {
        event.preventDefault();
        let enviar = validate(values);
        console.log("values", values)
        console.log("enviar", enviar)
        if( enviar ){
          let json_cursos = {}
          if(tipoForm.startsWith('Editar')){
            console.log("contenido", data_contenido)
            await axios.put(`http://127.0.0.1:8000/cursos/api/Curso/${data_contenido.id}/`, {"nombre": values.nombreCurso}, { headers: { Authorization: 'Token '+context?.token }})
            .then(res => {
              console.log("Curso", res.data)
              json_cursos = res.data
            }).catch(function (error) {
              if(error.response.status==401){
                handleClickOpenSesion()
              }
            })
            let promises_update_modulos_actuales = [];
            let r_modulos = [];
            for(let url of data_contenido.active_modulos){
              console.log("url", url)
              promises_update_modulos_actuales.push(
                axios.patch(url, {"id_curso": ''}, { headers: { Authorization: 'Token '+context?.token }})
                .then(res => {
                  r_modulos.push(res.data);
                }).catch(function (error) {
                  if(error.response.status==401){
                    handleClickOpenSesion()
                  }
                })
              )
            }
            await Promise.all(promises_update_modulos_actuales).then(() => console.log("r_modulos", r_modulos));
          }else{          
            //let json_cursos = {}
            await axios.post("http://127.0.0.1:8000/cursos/api/Curso/", {"nombre": values.nombreCurso}, { headers: { Authorization: 'Token '+context?.token }})
            .then(res => {
              console.log("Curso", res.data)
              json_cursos = res.data
            }).catch(function (error) {
              if(error.response.status==401){
                handleClickOpenSesion()
              }
            });
          }
          console.log("json_cursos", json_cursos)
          let promises_update_modulos = [];
          let r_modulos = [];
          for(let value of modulos.targetKeys){
            console.log("targetKeys value", value)
            promises_update_modulos.push(
              axios.patch(`http://127.0.0.1:8000/cursos/api/Modulo/${value}/`, {"id_curso": json_cursos.id}, { headers: { Authorization: 'Token '+context?.token }})
              .then(res => {
                r_modulos.push(res.data);
              }).catch(function (error) {
                if(error.response.status==401){
                  handleClickOpenSesion()
                }
              })
            )
          }
          await Promise.all(promises_update_modulos).then(() => console.log("r_modulos", r_modulos))
          //abrirCerrarModalEditarCrear(2, {"tipo": "diapositiva"})
          abrirCerrarModalEditarCrear(2)
          getRegistry()
          // reload page
        }
    }
    /****************************************************************************CUESTIONARIO*/
    const [valuesCuestionario, setValuesCuestionario] = useState({});
    const [preguntas, setPreguntas] = useState();
    const [errorsFormCuestionario, setErrorsFormCuestionario] = useState({});
    const callModules = async () =>{
      var targetKeys = [];
      var mockData = [];
      await axios.get("http://127.0.0.1:8000/cursos/api/Modulo/", { headers: { Authorization: 'Token ' + context.token } }).then(response => {
          console.log("response.data", response.data)
          mockData = response.data.map(function(currentValue, index, array) {
            return {
              key: currentValue.id.toString(),
              title: currentValue.nombre,
              description: currentValue.nombre
            };
          });
          console.log("mockData",  mockData)
      }).catch(error => {
        console.log("error", error)
        if(error.response?.status==401){
          //childRefContainer.current.handleClickOpenSesion()
          setOpenSession(true)
        }
      })
      console.log("mockData", mockData)
      console.log("targetKeys", targetKeys)
      return {targetKeys, mockData}
    }
    useEffect(() => {
      if(modalStatus==true){
        console.log("······································································modalStatus")
        setValues(initialFormValues)
      }
      if(modalStatus==true && tipoForm.startsWith('Editar ')){
        console.log("valuesCuestionario ...", valuesCuestionario)
        async function get_data_edit(){
          console.log("······································································Editar")
          console.log("editCurrent", editCurrent)
          let get_curso = ""
          await axios.get("http://127.0.0.1:8000/cursos/api/Curso/"+editCurrent.id+"/", { headers: { Authorization: 'Token ' + context.token } }).then(response => {
              console.log("response.data edit", response.data)
              initialFormValues.nombreCurso = response.data.nombre
              //handleInputChange()
              setData_Contenido(response.data)
              get_curso =  response.data
              setValues(initialFormValues)
          }).catch(error => {
            console.log("error", error)
            if(error.response?.status==401){
              //childRefContainer.current.handleClickOpenSesion()
              setOpenSession(true)
            }
          })
          const { mockData, targetKeys} = await callModules()
          console.log("get_curso", get_curso)
          console.log("get_curso active_modulos", get_curso.active_modulos)
          const targetKeys2 = get_curso.active_modulos.map((target1, index1, array1)=>{
            let split_url = target1.split("/")
            console.log("split_url", split_url)
            return split_url.at(-2)
          })
          console.log("targetKeys2", targetKeys2)
          setModulos({ mockData, targetKeys: targetKeys2});
        }
        get_data_edit()
      }else if(modalStatus==true && tipoForm.startsWith('Agregar ')){
        async function get_data_curses_initial(){
          const { mockData, targetKeys} = await callModules()
          setModulos({ mockData, targetKeys});
        }
        get_data_curses_initial()
      }
    },[modalStatus, tipoForm]);
  
    
    const [modulos, setModulos] = useState([]);
    const filterOption = (inputValue, option) => option.description.indexOf(inputValue) > -1;
    const handleChange = targetKeys => {
      setModulos({ ...modulos, targetKeys });
    };
    const handleSearch = (dir, value) => {
      console.log('search:', dir, value);
    };
    return (
        <>
          <Modal
              open={modalStatus}
              className={classes.modal}
              closeAfterTransition={false}
              BackdropComponent={Backdrop}
              BackdropProps={{
                  timeout: 0,
              }}
              onClose={abrirCerrarModalEditarCrear}>
                  <div className={classes.paperDiv}>
                    <Container component="main" className={classes.container}>
                      <CssBaseline />
                      <div className={classes.paper}>
                        <Grid container justifyContent="center">
                          <Grid item xs={12}>
                            <Typography component="h5" variant="h5">{tipoForm}</Typography>
                          </Grid>
                          <Grid item xs={12}>
                            <form className={classes.form} noValidate>
                              <TextField
                                  variant="outlined"
                                  margin="normal"
                                  fullWidth
                                  id="nombreCurso"
                                  label="Nombre Curso"
                                  name="nombreCurso"
                                  autoComplete="nombreCurso"
                                  autoFocus
                                  onBlur={handleInputChange}
                                  onChange={handleInputChange}
                                  value={values.nombreCurso}
                                  required
                                  {...(errorsForm["nombreCurso"] && {
                                  error: true,
                                  helperText: errorsForm["nombreCurso"]
                                  })}
                              />
                            </form>
                          </Grid>
                          <Grid container justifyContent="center">
                            <Transfer
                              dataSource={modulos.mockData}
                              showSearch
                              filterOption={filterOption}
                              targetKeys={modulos.targetKeys}
                              onChange={handleChange}
                              onSearch={handleSearch}
                              render={item => item.title}
                            />
                           </Grid>
                           <Grid container >
                              {
                                ( (tipoForm.startsWith('Agregar') && permisions.add) || (tipoForm.startsWith('Editar') && permisions.change) ) &&
                                      <Button
                                          onClick={handleSubmit}
                                          type="submit"
                                          fullWidth
                                          variant="contained"
                                          color="primary"
                                          className={classes.submit}
                                      >
                                        Guardar
                                      </Button>
                              }
                           </Grid>
                           <Grid container>
                            {/*
                              <Button variant="outlined" href="#outlined-buttons">
                                Agregar Modulos
                              </Button>
                            */}
                           </Grid>
                        </Grid>
                      </div>
                    </Container>
                    <DialogMsg open={openSesion} handleClose={handleCloseSesion} ttitle="Importante" msg="Sesión caducada"/>
                  </div>
          </Modal>
        </>
    )
}
export default EditarCrearCurses
