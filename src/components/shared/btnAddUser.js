import React, { useEffect, useContext, useState } from 'react';
import userContext from "../../context/userContext"
import {IconButton} from '@material-ui/core';
import {PersonAdd} from  '@material-ui/icons';

function BtnAddUser(props) {
    let { row, handleClickOpenModal, setRowSelected} = props
 
    function action(index){
        setRowSelected(index)
        handleClickOpenModal()
    }

    return (
        <>
            <IconButton onClick={() => { action(row); }}>
                <PersonAdd color="primary" />
            </IconButton>
        </>
    )
}
export default BtnAddUser