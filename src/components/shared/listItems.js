import React, { useState, useEffect, useMemo, useContext } from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import DashboardIcon from '@material-ui/icons/Dashboard';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import PeopleIcon from '@material-ui/icons/People';
import BarChartIcon from '@material-ui/icons/BarChart';
import LayersIcon from '@material-ui/icons/Layers';
import AssignmentIcon from '@material-ui/icons/Assignment';
import asyncLocalStorage from './../../utils/asyncLocalStorage'
import userContext from "./../../context/userContext"
import { Link } from 'react-router-dom';

import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import TocIcon from '@material-ui/icons/Toc';
import ClearAllIcon from '@material-ui/icons/ClearAll';
import VideoCallIcon from '@material-ui/icons/VideoCall';
import GroupAddIcon from '@material-ui/icons/GroupAdd';

const urls = {
  "usercustom": {
    "nombre": "Usuarios",
    "url": "/Home/users",
    "icon": "PeopleAltIcon"
  },
  "diplomados": {
    "nombre": "Diplomados",
    "url": "/Home/diplomados",
    "icon": "TocIcon"
  },
  "cursos": {
    "nombre": "Cursos",
    "url": "/Home/curses",
    "icon": "ClearAllIcon"
  },
  "modulo": {
    "nombre": "Modulos",
    "url": "/Home/modules",
    "icon": "VideoCallIcon"
  },
  "grupos_interno": {
    "nombre": "Grupos Interno",
    "url": "/Home/internal_group",
    "icon": "GroupAddIcon"
  },
}
export default function ListItems(props) {
  const [context, setContext] = useContext(userContext);

  return (
    <div>
          {context?.groups[0].permisosview.map((currentValue, index, array) => {
            let nombre = currentValue.obj_permisos_view.obj_permisos_option_view.nombre
            if( Object.keys(urls).includes(nombre) && currentValue.obj_permisos_view.view == true ){
              let icon = ""
              if(urls[nombre].icon=="PeopleAltIcon"){
                icon = (<PeopleAltIcon/>)
              }else if(urls[nombre].icon=="VideoCallIcon"){
                icon = (<VideoCallIcon/>)
              }else if(urls[nombre].icon=="TocIcon"){
                icon = (<TocIcon/>)
              }else if(urls[nombre].icon=="ClearAllIcon"){
                icon = ( <ClearAllIcon/>)
              }else if(urls[nombre].icon=="GroupAddIcon"){
                icon = ( <GroupAddIcon/>)
              }else{
                icon = (<DashboardIcon/>)
              }
              return (<ListItem button component={Link} to={urls[nombre].url} key={"ListItem"+index}>
                        <ListItemIcon>
                          {icon}
                        </ListItemIcon>
                        <ListItemText primary={urls[nombre].nombre} />
                      </ListItem>)
            }
          })}

    </div>
  )
}