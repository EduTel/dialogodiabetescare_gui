import React, { useEffect, useContext, useState } from 'react';
import userContext from "./../../context/userContext"
//edit
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import green from '@material-ui/core/colors/green';
import IconButton from '@material-ui/core/IconButton';
import RemoveIcon from '@material-ui/icons/Remove';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import Paper from '@material-ui/core/Paper';
import { v4 as uuidv4 } from 'uuid';

import axios from 'axios';

import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import { FilledInput } from '@material-ui/core';
//import RaisedButton from '@material-ui/RaisedButton';

import asyncLocalStorage from '../../utils/asyncLocalStorage'
import DialogMsg from "./dialogMsg"

import { Transfer } from 'antd';
import 'antd/dist/antd.css';

const useStyles = makeStyles((theme) => ({
  paperDiv: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '80%',
    maxHeight: '100%',
    overflow: 'auto',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  container: {
    width: '100%',
    padding: 0,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
  },
  paper: {
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
  },
}));
function AddGroup(props) {
    const {modalStatus, abrirCerrarModalEditarCrear, permisions, editCurrent, openSesion, setOpenSession, type} = props;
    const { v4: uuidv4 } = require('uuid');
    const [data_contenido, setData_Contenido] = useState([]);
    const [context, setContext] = useContext(userContext);
    const [errors, setErrors] = useState(false);
    const [errorsMsg, setErrorsMsg] = useState("");
    const [errorsForm, setErrorsForm] = useState({});
    const [values, setValues] = useState();
    //const [openSesion, setOpenSession] = useState(false);
    const classes = useStyles();
    const handleCloseSesion = () => {
      asyncLocalStorage.clear().then(function () {
          window.location.href = '/'
      })
    };
    const handleClickOpenSesion = () => {
      setOpenSession(true);
    };
    const handleSubmit = async (event) => {
        event.preventDefault();
        let edit = ""
        if(type==="obj_modulo"){
          edit = "id_modulo"
        }else if(type==="obj_curso"){
          edit = "id_curso"
        }else if(type==="obj_diplomado"){
          edit = "id_diplomado"
        }
        console.log("contenido", data_contenido)
        let deleteCurrent = [];
        let r_1 = [];
        for(let row of data_contenido){
          console.log("row", row.id)
          deleteCurrent.push(
            axios.delete(`http://127.0.0.1:8000/cursos/api/AccessData/${row.id}`, { headers: { Authorization: 'Token '+context?.token }})
            .then(res => {
              r_1.push(res.data);
            }).catch(function (error) {
              if(error.response.status==401){
                handleClickOpenSesion()
              }
            })
          )
        }
        await Promise.all(deleteCurrent).then(() => console.log("r_1", r_1));
        let promises_update_modulos = [];
        let r_2 = [];
        for(let value of modulos.targetKeys){
          console.log("targetKeys value", value)
          promises_update_modulos.push(
            axios.post(`http://127.0.0.1:8000/cursos/api/AccessData/`, {"id_grupos_interno": value,  [edit]: editCurrent.id}, { headers: { Authorization: 'Token '+context?.token }})
            .then(res => {
              r_2.push(res.data);
            }).catch(function (error) {
              if(error.response.status==401){
                handleClickOpenSesion()
              }
            })
          )
        }
        await Promise.all(promises_update_modulos).then(() => console.log("r_2", r_2))
        abrirCerrarModalEditarCrear()
    }
    /****************************************************************************CUESTIONARIO*/
    const [preguntas, setPreguntas] = useState();
    const [errorsFormCuestionario, setErrorsFormCuestionario] = useState({});
    const callAll = async () =>{
      var targetKeys = [];
      var mockData = [];
      await axios.get("http://127.0.0.1:8000/cursos/api/GruposInterno", { headers: { Authorization: 'Token ' + context.token } }).then(response => {
          console.log("response.data", response.data)
          mockData = response.data.map(function(currentValue, index, array) {
            return {
              key: currentValue.id.toString(),
              title: currentValue.nombre,
              description: currentValue.nombre
            };
          });
          console.log("mockData",  mockData)
      }).catch(error => {
        console.log("error", error)
        if(error.response?.status==401){
          //childRefContainer.current.handleClickOpenSesion()
          setOpenSession(true)
        }
      })
      console.log("mockData", mockData)
      console.log("targetKeys", targetKeys)
      return {targetKeys, mockData}
    }
    useEffect(() => {
      if(modalStatus==true){
        async function get_data(){
          let get_data = ""
          await axios.get(`http://127.0.0.1:8000/cursos/api/AccessData?obj_grupos_interno=0&${type}=${editCurrent?.id}`, { headers: { Authorization: 'Token ' + context.token } }).then(response => {
              console.log("response.data edit", response.data)
              setData_Contenido(response.data)
              get_data =  response.data
          }).catch(error => {
            console.log("error", error)
            if(error.response?.status==401){
              //childRefContainer.current.handleClickOpenSesion()
              setOpenSession(true)
            }
          })
          const { mockData, targetKeys} = await callAll()
          console.log("get_data", get_data)
          const targetKeys2 = get_data.map((target1, index1, array1)=>{
            console.log("split_url" , target1.obj_grupos_interno.id)
            return target1.obj_grupos_interno.id.toString()
          })
          console.log("targetKeys2", targetKeys2)
          setModulos({ mockData, targetKeys: targetKeys2});
        }
        get_data()
      }
    },[modalStatus]);
  
    
    const [modulos, setModulos] = useState([]);
    const filterOption = (inputValue, option) => option.description.indexOf(inputValue) > -1;
    const handleChange = targetKeys => {
      setModulos({ ...modulos, targetKeys });
    };
    const handleSearch = (dir, value) => {
      console.log('search:', dir, value);
    };
    return (
        <>
          <Modal
              open={modalStatus}
              className={classes.modal}
              closeAfterTransition={false}
              BackdropComponent={Backdrop}
              BackdropProps={{
                  timeout: 0,
              }}
              onClose={abrirCerrarModalEditarCrear}>
                  <div className={classes.paperDiv}>
                    <Container component="main" className={classes.container}>
                      <CssBaseline />
                      <div className={classes.paper}>
                        <Grid container justifyContent="center">
                          <Grid item xs={12}>
                            <Typography component="h5" variant="h5">Agregar Eliminar Grupos</Typography>
                          </Grid>
                          <Grid item xs={12}>
                          <Typography component="h6" variant="h6">{editCurrent?.nombre}</Typography>
                          </Grid>
                          <Grid container justifyContent="center">
                            <Transfer
                              dataSource={modulos.mockData}
                              showSearch
                              filterOption={filterOption}
                              targetKeys={modulos.targetKeys}
                              onChange={handleChange}
                              onSearch={handleSearch}
                              render={item => item.title}
                            />
                           </Grid>
                           <Grid container >
                              {
                                ( (permisions.add) || (permisions.change) ) &&
                                      <Button
                                          onClick={handleSubmit}
                                          type="submit"
                                          fullWidth
                                          variant="contained"
                                          color="primary"
                                          className={classes.submit}
                                      >
                                        Guardar
                                      </Button>
                              }
                           </Grid>
                           <Grid container>
                            {/*
                              <Button variant="outlined" href="#outlined-buttons">
                                Agregar Modulos
                              </Button>
                            */}
                           </Grid>
                        </Grid>
                      </div>
                    </Container>
                    <DialogMsg open={openSesion} handleClose={handleCloseSesion} ttitle="Importante" msg="Sesión caducada"/>
                  </div>
          </Modal>
        </>
    )
}
export default AddGroup