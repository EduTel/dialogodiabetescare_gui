import React, { useEffect, useContext, useState } from 'react';
import userContext from "../../context/userContext"
import {IconButton} from '@material-ui/core';
import {Delete} from  '@material-ui/icons';

function BtnDelete(props) {
    let { row, setMsg, handleClickOpenModal, setRowSelected} = props
 
    function action(index){
        setRowSelected(index)
        setMsg("Seguro que quiere eliminar el registro "+index)
        handleClickOpenModal()
    }

    return (
        <>
            <IconButton onClick={() => { action(row); }}>
                <Delete color="secondary" />
            </IconButton>
        </>
    )
}
export default BtnDelete