import React, { useState, useEffect, useContext } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom";
import './../../static/css/header.css';

import NotFound from '../notFound';
import ScreenHome from './../private/screenHome'
import ScreenLogin from './../screenLogin'
import ScreenProfile from './../private/screenProfile'

import ScreenDiplomados from '../private/screenDiplomados'
import ScreenCourses from '../private/screenCurses'
import ScreenModulos from '../private/screenModulos'
import ScreenInternalGroups from '../private/screenInternalGroups/screenInternalGroups'

import ScreenUsuarios from './../private/screenUsuarios'
//import ScreenModulosPermisos from './../private/screenModulosPermisos'
import OlvideMiPassword from './../olvideMiPassword'
import HeaderMenu from './headerMenu'
import SimpleMenu from './simpleMenu'
import SignUp from './../SignUp'
import ValidarRegistro from './../validarRegistro'
import Legal from './../legal/legal'
import Privacidad from './../legal/privacidad'
import Cookies from './../legal/cookies'

import userContext from "./../../context/userContext"
import { useHistory, withRouter, useLocation } from "react-router-dom";

import {mainListItems} from './listItems'
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import asyncLocalStorage from './../../utils/asyncLocalStorage'


const drawerWidth = 240;
const useStyles = makeStyles((theme) => ({
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
}));

function FilterRoute (props) {
  const {path, validate, loading, exact} = props
  console.log("_________________________FilterRoute:")
  console.log("_________________________loading: ", loading)
  console.log("_________________________path: ", path)

  let x = ""
  if(validate=="private"){
    if(loading==undefined){
      console.log(11111111111111)
    } else if(loading){
      x = props.children
    }else{
      x = <Redirect to={{pathname: '/'}} />
    }
  }else{
    if(loading==undefined){
      console.log(22222222222222)
    } else if(loading){
      x = <Redirect to={{pathname: '/home'}} />
    }else{
      x = props.children
    }
  }

  return exact?(<Route path={path}> {x} </Route>):(<Route exact path={path}> {x} </Route>)
}

function Header(props) {
    const [context, setContext] = useContext(userContext);
    const [loading, setLoading] = useState(undefined)
    //let history = useHistory()
    useEffect(() => {
      console.log("*******************useEffect localStorage_dialogodiabetescare")
      asyncLocalStorage.getItem('dialogodiabetescare').then(function (res) {
        console.log("________________________res: ",res)
        if(res===null){
          setLoading(false)
        }else{
          res = JSON.parse(res);
          //console.log(res)
          setContext(res)
          setLoading(true)
        }
      })
    },[]);
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const handleDrawerOpen = () => {
      setOpen(true);
    };
    const handleDrawerClose = () => {
      setOpen(false);
    };
    ////const location = useLocation()
    //const history = useHistory()
    return (
      <>
        <Router>
          <Switch>
            {/*
            <Route exact path="/Home/profile">
              <HeaderMenu handleDrawerOpen={handleDrawerOpen} handleDrawerClose={handleDrawerClose} open={open}>
                <ScreenProfile />
              </HeaderMenu>
            </Route>
            <Route exact path="/Home">
              <HeaderMenu handleDrawerOpen={handleDrawerOpen} handleDrawerClose={handleDrawerClose} open={open}>
                <ScreenHome />
              </HeaderMenu>
            </Route>
            */}
            <FilterRoute exact={true} path='/Home/diplomados' validate="private" loading={loading}>
              <HeaderMenu handleDrawerOpen={handleDrawerOpen} handleDrawerClose={handleDrawerClose} open={open}>
                <ScreenDiplomados />
              </HeaderMenu>
            </FilterRoute>
            <FilterRoute exact={true} path='/Home/curses' validate="private" loading={loading}>
              <HeaderMenu handleDrawerOpen={handleDrawerOpen} handleDrawerClose={handleDrawerClose} open={open}>
                <ScreenCourses />
              </HeaderMenu>
            </FilterRoute>
            <FilterRoute exact={true} path='/Home/modules' validate="private" loading={loading}>
              <HeaderMenu handleDrawerOpen={handleDrawerOpen} handleDrawerClose={handleDrawerClose} open={open}>
                <ScreenModulos />
              </HeaderMenu>
            </FilterRoute>
            <FilterRoute exact={true} path='/Home/internal_group' validate="private" loading={loading}>
              <HeaderMenu handleDrawerOpen={handleDrawerOpen} handleDrawerClose={handleDrawerClose} open={open}>
                <ScreenInternalGroups />
              </HeaderMenu>
            </FilterRoute>

            {/*
            <FilterRoute exact={true} path='/Home/modulespermissions' validate="private" loading={loading}>
              <HeaderMenu handleDrawerOpen={handleDrawerOpen} handleDrawerClose={handleDrawerClose} open={open}>
                <ScreenModulosPermisos />
              </HeaderMenu>
            </FilterRoute>
            */}
            <FilterRoute exact={true} path='/Home/users' validate="private" loading={loading}>
              <HeaderMenu handleDrawerOpen={handleDrawerOpen} handleDrawerClose={handleDrawerClose} open={open}>
                <ScreenUsuarios />
              </HeaderMenu>
            </FilterRoute>
            <FilterRoute exact={true} path='/Home/profile' validate="private" loading={loading}>
              <HeaderMenu handleDrawerOpen={handleDrawerOpen} handleDrawerClose={handleDrawerClose} open={open}>
                <ScreenProfile />
              </HeaderMenu>
            </FilterRoute>
            <FilterRoute path='/home' validate="private" loading={loading}>
              <HeaderMenu handleDrawerOpen={handleDrawerOpen} handleDrawerClose={handleDrawerClose} open={open}>
                <ScreenHome />
              </HeaderMenu>
            </FilterRoute>
            {/*
            <FilterRoute path='/admin' validate="private" loading={loading}>
              <HeaderMenu handleDrawerOpen={handleDrawerOpen} handleDrawerClose={handleDrawerClose} open={open}>
                  <ScreenAdmin />
                </HeaderMenu>
            </FilterRoute>
            */}
            <FilterRoute path='/CrearCuenta' validate="public" loading={loading}>
              <SimpleMenu handleDrawerOpen={handleDrawerOpen} handleDrawerClose={handleDrawerClose} open={open}>
                <SignUp />
              </SimpleMenu>
            </FilterRoute>
            <FilterRoute path='/legal' validate="public" loading={loading}>
              <SimpleMenu handleDrawerOpen={handleDrawerOpen} handleDrawerClose={handleDrawerClose} open={open}>
                <Legal />
              </SimpleMenu>
            </FilterRoute>
            <FilterRoute path='/privacidad' validate="public" loading={loading}>
              <SimpleMenu handleDrawerOpen={handleDrawerOpen} handleDrawerClose={handleDrawerClose} open={open}>
                <Privacidad />
              </SimpleMenu>
            </FilterRoute>
            <FilterRoute path='/OlvideMiPassword' validate="public" loading={loading}>
              <SimpleMenu handleDrawerOpen={handleDrawerOpen} handleDrawerClose={handleDrawerClose} open={open}>
                <OlvideMiPassword />
              </SimpleMenu>
            </FilterRoute>
            <FilterRoute path='/cookies' validate="public" loading={loading}>
              <SimpleMenu handleDrawerOpen={handleDrawerOpen} handleDrawerClose={handleDrawerClose} open={open}>
                <Cookies />
              </SimpleMenu>
            </FilterRoute>
            {/*
            <Route exact path="/CrearCuenta">
              <SimpleMenu handleDrawerOpen={handleDrawerOpen} handleDrawerClose={handleDrawerClose} open={open}>
                <SignUp />
              </SimpleMenu>
            </Route>
            <Route exact path="/legal">
              <SimpleMenu handleDrawerOpen={handleDrawerOpen} handleDrawerClose={handleDrawerClose} open={open}>
                <Legal />
              </SimpleMenu>
            </Route>
            <Route exact path="/privacidad">
              <SimpleMenu handleDrawerOpen={handleDrawerOpen} handleDrawerClose={handleDrawerClose} open={open}>
                <Privacidad />
              </SimpleMenu>
            </Route>
            <Route exact path="/OlvideMiPassword">
              <SimpleMenu handleDrawerOpen={handleDrawerOpen} handleDrawerClose={handleDrawerClose} open={open}>
                <OlvideMiPassword />
              </SimpleMenu>
            </Route>
            <Route exact path="/cookies">
              <SimpleMenu handleDrawerOpen={handleDrawerOpen} handleDrawerClose={handleDrawerClose} open={open}>
                <Cookies />
              </SimpleMenu>
            </Route>
            <Route exact path="/">
              <SimpleMenu handleDrawerOpen={handleDrawerOpen} handleDrawerClose={handleDrawerClose} open={open}>
                <ScreenLogin />
              </SimpleMenu>
            </Route>
            */}
            <FilterRoute path='/validar_registro/:token' validate="public" loading={loading}>
              <SimpleMenu handleDrawerOpen={handleDrawerOpen} handleDrawerClose={handleDrawerClose} open={open}>
                <ValidarRegistro/>
              </SimpleMenu>
            </FilterRoute>
            <FilterRoute path='/' validate="public" loading={loading}>
              <SimpleMenu handleDrawerOpen={handleDrawerOpen} handleDrawerClose={handleDrawerClose} open={open}>
                <ScreenLogin />
              </SimpleMenu>
            </FilterRoute>
            <Route>
              <SimpleMenu handleDrawerOpen={handleDrawerOpen} handleDrawerClose={handleDrawerClose} open={open}>
                <NotFound />
              </SimpleMenu>
            </Route>
          </Switch>
        </Router>
      </>
    )
}
export default Header