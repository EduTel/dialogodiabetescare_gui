import React, { useEffect, useContext, useState } from 'react';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import { useHistory } from "react-router-dom";

function DialogMsg(props) {
    const {open,handleClose,ttitle,msg} = props;
    //console.log("open", open)
    const [table, setTable] = useState([]);
    let history = useHistory();

    return (
        <>
          <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">{ttitle}</DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-dialog-description">
                  {msg}
                </DialogContentText>
              </DialogContent>
            <DialogActions>
              {props.children}
              <Button onClick={handleClose} color="primary" autoFocus>
                Cerrar
              </Button>
            </DialogActions>
          </Dialog>
        </>
    )
}
export default DialogMsg