import React, { useEffect, useContext, useState } from 'react';
import userContext from "../../context/userContext"
import './../../static/css/screenHome.css';
import './../../static/css/template.css';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import BtnDelete from './btnDelete';
import BtnEdit from './btnEdit';
import DialogMsg from './dialogMsg';
import axios from 'axios';

const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 440,
    },
});

function TableCrud(props) {
    const {handleClickOpenSesion, columns, url_base, rows, setData, abrirCerrarModalEditarCrear, deleteRegistryParent } = props; 
    const classes = useStyles();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const [msg, setMsg] = useState();
    const [open, setOpen] = React.useState(false);
    const [rowSelected, setRowSelected] = useState();
    const [context, setContext] = useContext(userContext);
  
    const handleClickOpenDelete = () => {
        setOpen(true);
    };

    const handleCloseDelete = () => {
        setOpen(false);
    };

    const deleteRegistry = () => {
        if(deleteRegistryParent!=undefined){
            deleteRegistryParent(rows, rowSelected)
        }else{
            axios.delete(url_base+rowSelected+"/", { headers: { Authorization: 'Token ' + context.token } }).then(response => {
                console.log("response.data", response.data)
                console.log("rowSelected", rowSelected)
                console.log(typeof(rows))
                console.log(rows)
                let rows_copy = rows.filter(row => row["id"] != rowSelected)
                console.log(rows_copy)
                setData(rows_copy)
            })
            .catch(error => {
                // Capturamos los errores
                console.log("error.request.responseText", typeof error.request?.responseText);
                console.log(error.message);
                if(error.response.status==401){
                    handleClickOpenSesion()
                }
                //else if(error.response.status==404){
                //    let rows_copy = rows.filter(row => row["id"] != rowSelected)
                //    console.log(rows_copy)
                //    setData(rows_copy)
                //}
            })
        }
        setOpen(false);
    };

    const handleChangePage = (event, newPage) => {
      setPage(newPage);
    };
  
    const handleChangeRowsPerPage = (event) => {
      setRowsPerPage(+event.target.value);
      setPage(0);
    };

    return (
        <>
            <Paper className={classes.root}>
                <TableContainer className={classes.container}>
                    <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                            <TableRow>
                            {columns.map((column) => (
                                <TableCell
                                    key={column.id}
                                    align={column.align}
                                    style={{ minWidth: column.minWidth }}
                                    >
                                {column.label}
                                </TableCell>
                            ))}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
                            return (
                                <TableRow hover role="checkbox" tabIndex={-1} key={"table"+row.id}>
                                {columns.map((column) => {
                                    const value = row[column.id];
                                    let imprimir = "" 
                                    if(column.id == "eliminar"){
                                        imprimir = <BtnDelete row={row.id} setMsg={setMsg} handleClickOpenModal={handleClickOpenDelete} setRowSelected={setRowSelected}/>
                                    }else if(column.id == "detalle"){
                                        imprimir = <BtnEdit row={row} url_base={url_base} handleClickOpenModal={abrirCerrarModalEditarCrear}/>
                                    }else if(column.type){
                                        imprimir = React.cloneElement(column.type, { row: row })
                                    }else if(column.format && typeof value === 'number'){
                                        imprimir = column.format(value)
                                    }else{
                                        imprimir = value
                                    }
                                    return (
                                        <TableCell key={column.id} align={column.align}>
                                            { imprimir }
                                        </TableCell>
                                    );
                                })}
                                </TableRow>
                            );
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[10, 25, 100]}
                    component="div"
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                />
                <DialogMsg open={open} handleClose={handleCloseDelete} ttitle="Importante" msg={msg}>
                    <Button onClick={deleteRegistry} color="secondary" autoFocus>
                        Aceptar
                    </Button>
                </DialogMsg>
            </Paper>
        </>
    )
}
export default TableCrud