import React, { useEffect, useContext, useState } from 'react';
import userContext from "../../context/userContext"
import {IconButton} from '@material-ui/core';
import {Slideshow} from  '@material-ui/icons';

function BtnView(props) {
    let { row, handleClickOpenModal, setRowSelected} = props
 
    function action(index){
        console.log("editCurrent",index)
        setRowSelected(index)
        handleClickOpenModal()
    }

    return (
        <>
            <IconButton onClick={() => { action(row); }}>
                <Slideshow color="primary" />
            </IconButton>
        </>
    )
}
export default BtnView