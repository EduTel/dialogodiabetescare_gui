import React, { useEffect, useContext, useState } from 'react';
import userContext from "../../context/userContext"
import {IconButton} from '@material-ui/core';
import VisibilityIcon from '@material-ui/icons/Visibility';

function BtnEdit(props) {
    let { row, handleClickOpenModal } = props
    const [context, setContext] = useContext(userContext);
    const [table, setTable] = useState([]);
    function action(index){
        console.log(index)
        handleClickOpenModal(1, row)
    }
    return (
        <>
            <IconButton onClick={() => { action(row); }}>
                <VisibilityIcon color="primary" />
            </IconButton>
        </>
    )
}
export default BtnEdit