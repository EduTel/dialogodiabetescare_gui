import React, { useEffect, useContext, useState } from 'react';
import userContext from "./../../../context/userContext"
//edit
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import green from '@material-ui/core/colors/green';
import IconButton from '@material-ui/core/IconButton';
import RemoveIcon from '@material-ui/icons/Remove';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import Paper from '@material-ui/core/Paper';
import { v4 as uuidv4 } from 'uuid';

import axios from 'axios';

import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import { FilledInput } from '@material-ui/core';
//import RaisedButton from '@material-ui/RaisedButton';

import asyncLocalStorage from '../../../utils/asyncLocalStorage'
import DialogMsg from "./../dialogMsg"

import EditarCrearCuestionario from "./editarCrearCuestionario"

const useStyles = makeStyles((theme) => ({
  paperDiv: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '80%',
    maxHeight: '100%',
    overflow: 'auto',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  container: {
    width: '100%',
    padding: 0,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
  },
  paper: {
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
  },
}));
function compare( a, b ) {
  if ( parseInt(a.position) < parseInt(b.position) ){
    return -1;
  }
  if ( parseInt(a.position) > parseInt(b.position) ){
    return 1;
  }
  return 0;
}
function compare_2( a, b ) {
  if ( parseInt(a.id) < parseInt(b.id) ){
    return -1;
  }
  if ( parseInt(a.id) > parseInt(b.id) ){
    return 1;
  }
  return 0;
}
export async function get_preguntas(preguntas, context, handleClickOpenSesion){
  console.log("preguntas", preguntas)
  let promises = [];
  let r_questions = [];
  for(let url of preguntas){
    console.log("url", url)
    promises.push(
      axios.get(url, { headers: { Authorization: 'Token '+context.token }})
      .then(res => {
        res.data.id_item = "pre_"+uuidv4()
        r_questions.push(res.data);
      }).catch(function (error) {
        if(error.response.status==401){
          handleClickOpenSesion()
        }
      })
    )
  }
  let r_questions_setPreguntas = []
    await Promise.all(promises).then(() => {
    /*
      {
        "pre_0fa62549-5abe-496f-90e5-3d96a8b9385b": {
          value : "",
          respuestas : [{value: "", selected: true}
                        {value: "", selected: true}
                        {value: "", selected: true}
                        {value: "", selected: true}]
        }
      }
    */
    /*
      {
        "id": key,
        "respuestas": [
          {"value": "", "selected": true},
          {"value": "", "selected": false},
          {"value": "", "selected": false},
          {"value": "", "selected": false},
        ]
      }
    */
    console.log("r_questions", r_questions)
    r_questions =  r_questions.sort( compare_2 );
    console.log("r_questions", r_questions)
    for(let row of r_questions){
      r_questions_setPreguntas.push({
        "id": row.id_item,
        "respuestas": row.respuestas
      })
    }
  });
  promises = [];
  let r_questions_respuestas = {};
  for(let row of r_questions_setPreguntas){
    console.log("row.respuestas", row.respuestas)
    /***************************************respuestas*/
    for(let url of row.respuestas){
      console.log("url", url)
      promises.push(
        axios.get(url, { headers: { Authorization: 'Token '+context.token }})
        .then(res => {
          if(r_questions_respuestas[row.id]==undefined){
            r_questions_respuestas[row.id] = []
          }
          r_questions_respuestas[row.id].push(res.data);
        }).catch(function (error) {
          if(error.response.status==401){
            handleClickOpenSesion()
          }
        })
      )
    }
    /***************************************respuestas END*/
  }
  await Promise.all(promises).then(() => {
    //console.log("r_questions_setPreguntas", r_questions_setPreguntas)
    console.log("r_questions_respuestas",  r_questions_respuestas)
    for(let row of r_questions_setPreguntas){
      row["respuestas"] = r_questions_respuestas[row.id].map(function(currentValue, index, array){ return {"value": currentValue.respuesta, "selected": currentValue.status , "position": currentValue.position} }).sort( compare )
    }
  });
  console.log("r_questions_respuestas", r_questions_respuestas)
  console.log("r_questions", r_questions)
  return [r_questions_respuestas, r_questions]
}
function EditarCrearDiapositiva(props) {
    const {modalEditar, abrirCerrarModalEditarCrear, permisions, tipoForm, getRegistry, editCurrent, openSesion, setOpenSession} = props;
    const initialFormValues = {
        iframe: "",
        nombreModulo: ""
    }
    const { v4: uuidv4 } = require('uuid');
    const [modulo_contenido, setModulo_Contenido] = useState([]);
    const [sonidos, setSonidos] = useState([]);
    const [context, setContext] = useContext(userContext);
    const [errors, setErrors] = useState(false);
    const [errorsMsg, setErrorsMsg] = useState("");
    const [errorsForm, setErrorsForm] = useState({});
    const [values, setValues] = useState(initialFormValues);
    //const [openSesion, setOpenSession] = useState(false);
    const classes = useStyles();
    const handleCloseSesion = () => {
      asyncLocalStorage.clear().then(function () {
          window.location.href = '/'
      })
    };
    const handleClickOpenSesion = () => {
      setOpenSession(true);
    };
    const handleInputChange = (e) => {
        console.log("e", e)
        const { name, value } = e.target;
        setValues({
          ...values,
          [name]: value
        });
        validate({ [name]: value });
    }
    const validate = (fieldValues = values) => {
        console.log("=================================validate")
        console.log("fieldValues", fieldValues)
        console.log("values", values)
        let temp = { ...errorsForm };
        console.log("temp", temp)
    
        if ("iframe" in fieldValues) {
          temp.iframe = fieldValues.iframe ? "" : "El campo es requerido.";
        }
        if ("nombreModulo" in fieldValues) {
          console.log("fieldValues.nombreModulo", fieldValues.nombreModulo)
          temp.nombreModulo = fieldValues.nombreModulo ? "" : "El campo es requerido.";
        }
        
        for(let row in  fieldValues){
          if(row.startsWith('mu_')){
            console.log("row", row)
            temp[row] = fieldValues[row]  ? "" : "El campo es requerido.";
          }
        }
    
        console.log("temp", temp)
        let enviar = true
        for(let row_data in temp){
          if(temp[row_data] != ""){
            enviar = false
            break
          }
        }
        setErrorsForm({
          ...temp
        });
        console.log("=================================", enviar)
        return enviar
    };
    const inset_sound = async (json_diapositiva) => {
      //let sounds = {...values}
      let sounds = Object.assign({}, values)
      delete sounds.nombreModulo
      delete sounds.iframe
      console.log("sounds", sounds)
      let counter = 0
      let promises = [];
      let r_sounds = [];
      for(let key in sounds){
        console.log("sounds[key]", sounds[key])
        promises.push(
          axios.post("http://127.0.0.1:8000/cursos/api/Audio/", {"path": sounds[key], "position": counter, "id_diapositiva": json_diapositiva.id}, { headers: { Authorization: 'Token '+context?.token }})
          .then(res => {
            r_sounds.push(res.data);
          }).catch(function (error) {
            if(error.response.status==401){
              handleClickOpenSesion()
            }
          })
        )
        counter++
      }
      await Promise.all(promises).then(() => console.log("r_sounds", r_sounds));
    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        let enviar = validate(values);
        let enviarCuestionario = validateCuestionario(valuesCuestionario)
        enviar = (enviar && enviarCuestionario)
        //console.log(errorsForm)
        //let enviar = true
        //for(let row_data in errorsForm){
        //  if(errorsForm[row_data] != ""){
        //    enviar = false
        //    break
        //  }
        //}
        console.log("values", values)
        console.log("enviar", enviar)
        if( enviar ){
          let json_diapositiva = {}
          if(tipoForm=="Editar Diapositiva"){
            console.log("modulo_contenido", modulo_contenido)
            json_diapositiva = {"id": modulo_contenido.obj_diapositiva.id}
            await axios.patch("http://127.0.0.1:8000/cursos/api/Modulo/"+modulo_contenido.id+"/", {"nombre": values.nombreModulo}, { headers: { Authorization: 'Token '+context?.token }})
            .then(res => {
            }).catch(function (error) {
              if(error.response.status==401){
                handleClickOpenSesion()
              }
            })
            let diapositiva = ""
            await axios.patch("http://127.0.0.1:8000/cursos/api/Diapositiva/"+modulo_contenido.obj_diapositiva.id+"/", {"iframe": values.iframe}, { headers: { Authorization: 'Token '+context?.token }})
            .then(res => {
              diapositiva = res.data
            }).catch(function (error) {
              if(error.response.status==401){
                handleClickOpenSesion()
              }
            })
            let promises = [];
            for(let url of diapositiva.active_audios){
              console.log("url", url)
              promises.push(
                axios.delete(url, { headers: { Authorization: 'Token '+context.token }})
                .then(res => {
                }).catch(function (error) {
                  if(error.response.status==401){
                    handleClickOpenSesion()
                  }
                })
              )
            }
            await Promise.all(promises).then(() => {
              console.log("delete", "true")
            });
            let promises_active_preguntas = [];
            for(let url of diapositiva.active_preguntas){
              console.log("url", url)
              promises_active_preguntas.push(
                axios.delete(url, { headers: { Authorization: 'Token '+context.token }})
                .then(res => {
                }).catch(function (error) {
                  if(error.response.status==401){
                    handleClickOpenSesion()
                  }
                })
              )
            }
            await Promise.all(promises_active_preguntas).then(() => {
              console.log("delete", "true")
            });
          }else{          
            await axios.post("http://127.0.0.1:8000/cursos/api/Diapositiva/", {"iframe": values.iframe}, { headers: { Authorization: 'Token '+context?.token }})
            .then(res => {
              json_diapositiva = res.data
            }).catch(function (error) {
              if(error.response.status==401){
                handleClickOpenSesion()
              }
            })
            console.log("json_diapositiva", json_diapositiva)
            let json_modulo = 0
            await axios.post("http://127.0.0.1:8000/cursos/api/Modulo/", {"nombre": values.nombreModulo, "id_diapositiva": json_diapositiva.id}, { headers: { Authorization: 'Token '+context?.token }})
            .then(res => {
              json_modulo = res.data
            }).catch(function (error) {
              if(error.response.status==401){
                handleClickOpenSesion()
              }
            })
            console.log("json_modulo", json_modulo) 
          }
          await insert_cuestions(json_diapositiva) 
          await inset_sound(json_diapositiva)
          //abrirCerrarModalEditarCrear(2, {"tipo": "diapositiva"})
          abrirCerrarModalEditarCrear(2)
          getRegistry()
        }
    }
    const addMusic = () => {
      console.log("====================addMusic")
      const id4 = uuidv4()
      let key = "mu_"+id4
      console.log("key", key)
      let id = {"id_item": key}
      setSonidos([...sonidos, id])
      setValues({...values,...{[key]: ""}})
    }
    const deleteMusic = (obj, id4) => {
      console.log("====================deleteMusic")
      //console.log("obj", obj)
      console.log("id4", id4)
      //let id4 = obj.currentTarget.getAttribute('relate')
      //console.log("id4", id4)
      let new_sonidos = sonidos.filter(function (data) {
          return data.id_item!=id4; 
      });
      console.log("new_sonidos", new_sonidos)
      setSonidos(new_sonidos)
      let new_values = {...values}
      delete new_values[id4];
      console.log("new_values", new_values)
      setValues({...new_values})
    }
    /****************************************************************************CUESTIONARIO*/
    const [valuesCuestionario, setValuesCuestionario] = useState({});
    const [preguntas, setPreguntas] = useState();
    const [errorsFormCuestionario, setErrorsFormCuestionario] = useState({});
    const handleInputChangeCuestionario = (e, pregunta, respuesta) => {
      console.log("=================================================================handleInputChangeCuestionario")
      console.log("valuesCuestionario", valuesCuestionario)
      console.log("pregunta", pregunta)
      console.log("respuesta", respuesta)
      const { name, value } = e.target;
      console.log("name", name)
      console.log("value", value)
      if(name.startsWith('rgg_')){
        console.log("_______________________________________radio group")
        const pre = name.split('rgg_')
        console.log("pre",  pre[1])
        pregunta = pre[1]
        let nueva_respuesta_rb = {"selected": true}
        console.log("viejas respuesta", valuesCuestionario[pregunta]?.respuestas)
        console.log("valuesCuestionario[pregunta].respuestas[value]", valuesCuestionario[pregunta].respuestas[value])
        for(let row_respuestas in valuesCuestionario[pregunta].respuestas){
          console.log("row_respuestas", row_respuestas)
          //valuesCuestionario[pregunta].respuestas[row_respuestas] = { ...valuesCuestionario[pregunta].respuestas[value],...{"selected": false}}
          valuesCuestionario[pregunta].respuestas[row_respuestas].selected = false
        }
        valuesCuestionario[pregunta].respuestas[value] = { ...valuesCuestionario[pregunta].respuestas[value],...nueva_respuesta_rb}
        console.log("valuesCuestionario[pregunta].respuestas[value]", valuesCuestionario[pregunta].respuestas[value])
        console.log("handleInputChangeCuestionario valuesCuestionario 1", valuesCuestionario)
        setValuesCuestionario(valuesCuestionario);
      }else if(pregunta!=undefined && respuesta!=undefined ){
        console.log("____________________respuestas")
        console.log("1 values[pregunta]", valuesCuestionario[pregunta])
        //let nueva_pregunta = {[name]: value}
        let nueva_respuesta = {"value": value, "selected": true}
        console.log("nueva respuesta", nueva_respuesta)
        console.log("viejas respuesta", valuesCuestionario[pregunta]?.respuestas)
        if(valuesCuestionario[pregunta]==""){
          valuesCuestionario[pregunta] = {}
        }
        //let respuestas = {...valuesCuestionario[pregunta]?.respuestas, ...nueva_respuesta }
        //let respuestas = {...valuesCuestionario[pregunta]?.respuestas, ...nueva_respuesta }
        valuesCuestionario[pregunta].respuestas[respuesta] = nueva_respuesta
        //console.log("join", respuestas)
        //valuesCuestionario[pregunta]["respuestas"] = respuestas
        console.log("2 valuesCuestionario[pregunta]", valuesCuestionario[pregunta])
        //let sub_data = { ...valuesCuestionario[name], ...{...valuesCuestionario[name]?.preguntas, ...{"preguntas": value} } } 
        //console.log("sub_data", sub_data)
        //  ...valuesCuestionario,
        //  [name]: { ...valuesCuestionario[name], ...{"value": value} } 
        //}
        console.log("handleInputChangeCuestionario valuesCuestionario 2", valuesCuestionario)
        setValuesCuestionario(valuesCuestionario);
        validateCuestionario({[pregunta]:valuesCuestionario[pregunta]});
      }else{
        console.log("____________________preguntas")
        let sub_data = { ...valuesCuestionario[name], ...{"value": value} } 
        console.log("valuesCuestionario[name]", valuesCuestionario[name])
        console.log("sub_data", sub_data)
        let data =  {
          ...valuesCuestionario,
          [name]: { ...valuesCuestionario[name], ...{"value": value} }
        }
        console.log("data" , data)
        console.log("handleInputChangeCuestionario valuesCuestionario 3", data)
        setValuesCuestionario(data);
        validateCuestionario({ [name]: { "value": value } });
      }
    }
    const validateCuestionario = (fieldValues = valuesCuestionario) => {
      console.log("==================================================================validateCuestionario")
      console.log("fieldValues", fieldValues)
      let temp = { ...errorsFormCuestionario };
      console.log("temp", temp)
      for(let row in fieldValues){
        if(row.startsWith('pre_')){
          console.log("temp", temp)
          console.log("row", row)
          console.log("temp[row]",temp[row])
          if(temp[row]==undefined){
            temp[row] = {}
          }
          temp[row]["value"] = fieldValues[row].value  ? "" : "El campo es requerido.";
          for(let row2 in fieldValues[row]["respuestas"]){
            console.log("row2", fieldValues[row]["respuestas"][row2])
            if(temp[row]["respuestas"]==undefined){
              temp[row]["respuestas"] = {}
            }
            temp[row]["respuestas"][row2] = fieldValues[row]["respuestas"][row2].value  ? "" : "El campo es requerido.";
          }
        }
      }
      console.log("_temp", temp)
      let enviar = true
      loop1: for(let row_data in temp){
        console.log("row_data",row_data)
        console.log("temp[row_data]", temp[row_data])
        if(row_data.startsWith('pre_')){
          if(temp[row_data].value!=""){
            console.log("==============false 1")
            enviar = false
            break
          }
          for(let row2 in temp[row_data]?.["respuestas"]){
            console.log("temp[row_data]['respuestas'][row2]", temp[row_data]["respuestas"][row2])
            if(temp[row_data]["respuestas"][row2]!=""){
              console.log("==============false 1.1")
              enviar = false
              break loop1;
            }
          }
        }else if(temp[row_data]!=""){
          console.log("==============false 3")
          enviar = false
          break
        }
      }
      console.log("temp", temp)
      setErrorsFormCuestionario({
        ...temp
      });
      console.log("=================================", enviar)
      return enviar
    };
    const insert_cuestions = async (json_diapositiva) => {
      console.log("================================inset_cuestions")
      let cuestions = {...valuesCuestionario}
      console.log("cuestions", cuestions)

      let promises_respuestas = [];
      let r_respuesta = [];
      let position_question = 0
      for(let key in cuestions){
        console.log("***********************************cuestions[key]", cuestions[key])
        console.log("***********************************cuestions[key]", cuestions[key].value)
        //promises.push(
        axios.post("http://127.0.0.1:8000/cursos/api/Pregunta/", {
                                                                    "pregunta": cuestions[key].value,
                                                                    "id_diapositiva": json_diapositiva.id,
                                                                    "position": position_question
                                                                  }, { headers: { Authorization: 'Token '+context?.token }})
        .then(res => {
          console.log("cuestions[key].respuestas", cuestions[key].respuestas)
          console.log("res",  res.data.id)
          let position_respuesta = 0
          for(let respuesta of cuestions[key].respuestas){
            console.log("respuesta",  respuesta)
            promises_respuestas.push(
              axios.post("http://127.0.0.1:8000/cursos/api/Respuesta/", {
                                                                          "respuesta": respuesta.value, 
                                                                          "status": respuesta.selected,
                                                                          "id_pregunta": res.data.id,
                                                                          "position": position_respuesta
                                                                        }, { headers: { Authorization: 'Token '+context?.token }})
              .then(res => {
                r_respuesta.push(res.data);
              }).catch(function (error) {
                if(error.response.status==401){
                  handleClickOpenSesion()
                }
              })
            )
            position_respuesta++
          }

        }).catch(function (error) {
          if(error.response.status==401){
            handleClickOpenSesion()
          }
        })
        await Promise.all(promises_respuestas).then(() => console.log("r_respuesta", r_respuesta));
        position_question++
        //)
      }
    }
    const addPreguntas = () => {
      console.log("====================addPreguntas")
      const id4 = uuidv4()
      let key = "pre_"+id4
      console.log("key", key)
      let respuestas = [
        {"value": "", "selected": true },
        {"value": "", "selected": false},
        {"value": "", "selected": false},
        {"value": "", "selected": false},
      ]
      let obj_pregunta = {"id": key, "respuestas": respuestas, "defaultValue": "0"}
      console.log("obj_pregunta", obj_pregunta)
      console.log("[...preguntas, obj_pregunta]", [...preguntas, obj_pregunta])
      //if(preguntas==undefined){
      //  setPreguntas([obj_pregunta])
      //}else{
      //  setPreguntas([...preguntas, obj_pregunta])
      //}
      //setValuesCuestionario({...values,...{[key]: ""}})
      let valu = {...valuesCuestionario,...{[key]: {"respuestas": obj_pregunta["respuestas"], "defaultValue": obj_pregunta["defaultValue"]} }}
      console.log("addPreguntas setValuesCuestionario", valu)
      setValuesCuestionario(valu)
    }
    const deletePreguntas = (obj, id4) => {
      console.log("====================deletePreguntas")
      //let id4 = obj.currentTarget.getAttribute('relate')
      //console.log("id4", id4)
      let new_preguntas = preguntas.filter(function (data) {
          return data.id!=id4; 
      });
      console.log("deletePreguntas setPreguntas", new_preguntas)
      setPreguntas(new_preguntas)
      let new_values = {...valuesCuestionario}
      delete new_values[id4];
      console.log("deletePreguntas setValuesCuestionario", new_values)
      setValuesCuestionario({...new_values})
    }
    //const handleSubmitCuestionario = async (event) => {
    //  event.preventDefault();
    //  let enviar = validateCuestionario(valuesCuestionario);
    //  //console.log("errorsFormCuestionario: ", errorsFormCuestionario)
    //  //for(let row_data in errorsFormCuestionario){
    //  //  if(errorsFormCuestionario[row_data] != ""){
    //  //    enviar = false
    //  //    break
    //  //  }
    //  //}
    //  //if(valuesCuestionario==undefined){
    //  //  enviar = false
    //  //}else if(Object.keys(valuesCuestionario).length==0){
    //  //  enviar = false
    //  //}
    //  console.log("valuesCuestionario", valuesCuestionario)
    //  console.log("enviar", enviar)
    //  if( enviar ){
    //    let json_cuestionario = {}
    //    if(tipoForm=="Editar Cuestionario"){
    //    }else{
    //      let json_modulo = 0
    //      //console.log("json_cuestionario", json_cuestionario)
    //      //await axios.post("http://127.0.0.1:8000/cursos/api/Modulo_Contenido/", {"id_modulo": json_modulo.id, "id_cuestionario": json_cuestionario.id}, { headers: { Authorization: 'Token '+context?.token }})
    //      //.then(res => {
    //      //  console.log("Modulo", res)
    //      //}).catch(function (error) {
    //      //  if(error.response.status==401){
    //      //    handleClickOpenSesion()
    //      //  }
    //      //})
    //    }
    //    await insert_cuestions(json_cuestionario)
    //  }
    //}
    /*****************************************************************************/

    useEffect(() => {
      if(modalEditar==true){
        setValues(initialFormValues)
        setSonidos([])
        setPreguntas([])
      }
      if(modalEditar==true && tipoForm=="Editar Diapositiva"){
        console.log("valuesCuestionario ...", valuesCuestionario)
        async function get_data_edit(){
          console.log("······································································Editar")
          console.log("editCurrent", editCurrent)
          let diapositiva = ""
          await axios.get("http://127.0.0.1:8000/cursos/api/Modulo/"+editCurrent.id+"/", { headers: { Authorization: 'Token ' + context.token } }).then(response => {
              console.log("response.data edit", response.data)
              initialFormValues.nombreModulo = response.data.nombre
              initialFormValues.iframe = response.data.obj_diapositiva.iframe
              //handleInputChange()
              setModulo_Contenido(response.data)
              setValues(initialFormValues)
              const url_diapositiva = "http://127.0.0.1:8000/cursos/api/Diapositiva/"+response.data.obj_diapositiva.id+"/"
              console.log("url_diapositiva", url_diapositiva)
              return axios.get(url_diapositiva, { headers: { Authorization: 'Token ' + context.token } })
          }).then(res => {
            console.log("Diapositiva", res)
            diapositiva = res.data
          }).catch(error => {
            console.log("error", error)
            if(error.response?.status==401){
              //childRefContainer.current.handleClickOpenSesion()
              setOpenSession(true)
            }
          })
          /************************************************************************************************FILL sounds*/
          let promises = [];
          let r_sounds = [];
          for(let url of diapositiva.active_audios){
            console.log("url", url)
            promises.push(
              axios.get(url, { headers: { Authorization: 'Token '+context.token }})
              .then(res => {
                const id4 = uuidv4()
                let key = "mu_"+id4
                res.data.id_item = key
                r_sounds.push(res.data);
              }).catch(function (error) {
                if(error.response.status==401){
                  handleClickOpenSesion()
                }
              })
            )
          }
          await Promise.all(promises).then(() => {
            console.log("????????????????????????????????????????????????????????????Promise.al sounds")
            console.log("r_sounds", r_sounds)
            r_sounds =  r_sounds.sort( compare );
            console.log("r_sounds", r_sounds)
            setSonidos(r_sounds)
            let r_sounds_id_item = {}
            for(let row of r_sounds){
              r_sounds_id_item[row.id_item] = row.path
            }
            setValues({...initialFormValues,...r_sounds_id_item})
          });
          /************************************************************************************************FILL questions*/
          let data = await get_preguntas(diapositiva.active_preguntas, context, handleClickOpenSesion)
          let r_questions_respuestas =  data[0]
          let r_questions = data[1]
          //console.log("useEffect setPreguntas :)", r_questions_setPreguntas)
          let r_questions_setValuesCuestionario = {}
          for(let row of r_questions ){
            let respuestas = r_questions_respuestas[row.id_item].map(function(currentValue, index, array){ return {"value": currentValue.respuesta, "selected": currentValue.status , "position": currentValue.position} }).sort( compare )
            r_questions_setValuesCuestionario[row.id_item] = {
              "value": row.pregunta,
              "respuestas": respuestas,
              "defaultValue": respuestas.map(function(currentValue, index, array){ if(currentValue.selected==true){return index} }).filter(function(val) { if(val!=undefined){ return val }})[0]
            }
          }
          console.log("________________________________________________________________________")
          console.log("useEffect setValuesCuestionario", r_questions_setValuesCuestionario)
          //console.log("useEffect setPreguntas :)", r_questions_setPreguntas)
          //let valu = {...valuesCuestionario,...{[key]: {"respuestas": r_questions_id_item["respuestas"]} }}
          //console.log("valu", valu)
          setValuesCuestionario(r_questions_setValuesCuestionario)
          //setPreguntas(r_questions_setPreguntas)
        }
        get_data_edit()
      }
    },[modalEditar, tipoForm]);
    useEffect(() => {
      // este efect se ejecuta despues de valuesCuestionario
      let data = Object.keys(valuesCuestionario).map(function(currentValue, index, array){ return {"id": currentValue, "respuestas": valuesCuestionario[currentValue].respuestas}  })
      console.log("useEffect setPreguntas 2", data)
      setPreguntas( data )
    },[valuesCuestionario]);    
    return (
        <>
          <Modal
              open={modalEditar}
              className={classes.modal}
              closeAfterTransition={false}
              BackdropComponent={Backdrop}
              BackdropProps={{
                  timeout: 0,
              }}
              onClose={abrirCerrarModalEditarCrear}>
                  <div className={classes.paperDiv}>
                    <Container component="main" className={classes.container}>
                      <CssBaseline />
                      <div className={classes.paper}>
                          <Typography component="h5" variant="h5">{tipoForm}</Typography>
                          <form className={classes.form} noValidate>
                              <TextField
                                  variant="outlined"
                                  margin="normal"
                                  fullWidth
                                  id="nombreModulo"
                                  label="Nombre Modulo"
                                  name="nombreModulo"
                                  autoComplete="nombreModulo"
                                  autoFocus
                                  onBlur={handleInputChange}
                                  onChange={handleInputChange}
                                  value={values.nombreModulo}
                                  required
                                  {...(errorsForm["nombreModulo"] && {
                                  error: true,
                                  helperText: errorsForm["nombreModulo"]
                                  })}
                              />
                              <TextField
                                  variant="outlined"
                                  margin="normal"
                                  fullWidth
                                  id="iframe"
                                  label="Iframe"
                                  name="iframe"
                                  autoComplete="iframe"
                                  multiline
                                  rows={4}
                                  onBlur={handleInputChange}
                                  onChange={handleInputChange}
                                  value={values.iframe}
                                  required
                                  {...(errorsForm["iframe"] && {
                                  error: true,
                                  helperText: errorsForm["iframe"]
                                  })}
                              />
                              {/*
                              <IconButton onClick={() => { addMusic() }}>
                                  <AddCircleOutlineIcon color="primary" />
                              </IconButton>
                              */}
                              <Paper>
                              {
                                //sonidos.map(function(currElement, index) {
                                //  console.log("***************************************************sonidos", index+1)
                                //  console.log("index", index+1)
                                //  console.log("currElement.id_item", currElement.id_item)
                                //  return (
                                //    <Grid container spacing={0} key={currElement.id_item}>
                                //      <Grid item xs={11}>
                                //        <TextField
                                //          variant="outlined"
                                //          margin="normal"
                                //          fullWidth
                                //          id={currElement.id_item}
                                //          label={"sonido: "+ (parseInt(index)+1)}
                                //          name={currElement.id_item}
                                //          onBlur={handleInputChange}
                                //          onChange={handleInputChange}
                                //          value={values[currElement.id_item]}
                                //          required
                                //          {...(errorsForm[currElement.id_item] && {
                                //          error: true,
                                //          helperText: errorsForm[currElement.id_item]
                                //          })}
                                //        />
                                //      </Grid>
                                //      <Grid item xs={1}>
                                //        <IconButton onClick={(e) => { deleteMusic(e, currElement.id_item) }}>
                                //            <RemoveIcon color="secondary" />
                                //        </IconButton>
                                //      </Grid>
                                //    </Grid>
                                //  )
                                //})
                              }
                              </Paper>
                              <EditarCrearCuestionario
                              validateCuestionario={validateCuestionario}
                              preguntas={preguntas}
                              handleInputChangeCuestionario={handleInputChangeCuestionario}
                              insert_cuestions={insert_cuestions}
                              addPreguntas={addPreguntas}
                              deletePreguntas={deletePreguntas}
                              errorsFormCuestionario={errorsFormCuestionario}
                              valuesCuestionario={valuesCuestionario}
                              >
                              </EditarCrearCuestionario>
                              { errors &&
                                  <Typography variant="h5" component="h2" style={{ color: "red" }}>
                                      {errorsMsg}
                                  </Typography>
                              }
                              {
                                ( (tipoForm=="Agregar Diapositiva" && permisions.add) || (tipoForm=="Editar Diapositiva" && permisions.change) ) &&
                                      <Button
                                          onClick={handleSubmit}
                                          type="submit"
                                          fullWidth
                                          variant="contained"
                                          color="primary"
                                          className={classes.submit}
                                      >
                                        Guardar
                                      </Button>
                              }
                          </form>
                      </div>
                    </Container>
                    <DialogMsg open={openSesion} handleClose={handleCloseSesion} ttitle="Importante" msg="Sesión caducada"/>
                  </div>
          </Modal>
        </>
    )
}
export default EditarCrearDiapositiva
