import React, { useEffect, useContext, useState } from 'react';
import userContext from "./../../../context/userContext"
//edit
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import green from '@material-ui/core/colors/green';
import IconButton from '@material-ui/core/IconButton';
import RemoveIcon from '@material-ui/icons/Remove';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import Paper from '@material-ui/core/Paper';
import { v4 as uuidv4 } from 'uuid';

import axios from 'axios';

import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import { FilledInput } from '@material-ui/core';
//import RaisedButton from '@material-ui/RaisedButton';

import asyncLocalStorage from '../../../utils/asyncLocalStorage'
import DialogMsg from "./../dialogMsg"
import {get_preguntas} from "./editarCrearDiapositiva"

import { Transfer } from 'antd';
import 'antd/dist/antd.css';

const useStyles = makeStyles((theme) => ({
  paperDiv: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '80%',
    maxHeight: '100%',
    overflow: 'auto',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  container: {
    width: '100%',
    padding: 0,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
  },
  paper: {
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
  },
}));
function View(props) {
    const {modalStatus, abrirCerrarModalEditarCrear, permisions, editCurrent, openSesion, setOpenSession, type} = props;
    const { v4: uuidv4 } = require('uuid');
    const [data_contenido, setData_Contenido] = useState([]);
    const [context, setContext] = useContext(userContext);
    const [errors, setErrors] = useState(false);
    const [errorsMsg, setErrorsMsg] = useState("");
    const [errorsForm, setErrorsForm] = useState({});
    const [values, setValues] = useState();
    //const [openSesion, setOpenSession] = useState(false);
    const classes = useStyles();
    const handleCloseSesion = () => {
      asyncLocalStorage.clear().then(function () {
          window.location.href = '/'
      })
    };
    const handleClickOpenSesion = () => {
      setOpenSession(true);
    };
    const handleSubmit = async (event) => {
        event.preventDefault();
        let edit = ""
        if(type==="obj_modulo"){
          edit = "id_modulo"
        }else if(type==="obj_curso"){
          edit = "id_curso"
        }else if(type==="obj_diplomado"){
          edit = "id_diplomado"
        }
        console.log("contenido", data_contenido)
        let deleteCurrent = [];
        let r_1 = [];
        for(let row of data_contenido){
          console.log("row", row.id)
          deleteCurrent.push(
            axios.delete(`http://127.0.0.1:8000/cursos/api/AccessData/${row.id}`, { headers: { Authorization: 'Token '+context?.token }})
            .then(res => {
              r_1.push(res.data);
            }).catch(function (error) {
              if(error.response.status==401){
                handleClickOpenSesion()
              }
            })
          )
        }
        await Promise.all(deleteCurrent).then(() => console.log("r_1", r_1));
        let promises_update_modulos = [];
        let r_2 = [];
        for(let value of modulos.targetKeys){
          console.log("targetKeys value", value)
          promises_update_modulos.push(
            axios.post(`http://127.0.0.1:8000/cursos/api/AccessData/`, {"id_user_custom": value, [edit]: editCurrent.id}, { headers: { Authorization: 'Token '+context?.token }})
            .then(res => {
              r_2.push(res.data);
            }).catch(function (error) {
              if(error.response.status==401){
                handleClickOpenSesion()
              }
            })
          )
        }
        await Promise.all(promises_update_modulos).then(() => console.log("r_2", r_2))
        abrirCerrarModalEditarCrear()
    }
    /****************************************************************************CUESTIONARIO*/
    const [preguntas, setPreguntas] = useState();
    const [errorsFormCuestionario, setErrorsFormCuestionario] = useState({});
    function compare_2( a, b ) {
      if ( parseInt(a.id) < parseInt(b.id) ){
        return -1;
      }
      if ( parseInt(a.id) > parseInt(b.id) ){
        return 1;
      }
      return 0;
    }
    useEffect(() => {
      if(modalStatus==true){
        async function get_data(){
          console.log("editCurrent?.obj_diapositiva.active_preguntas", editCurrent?.obj_diapositiva.active_preguntas)
          let data = await get_preguntas(editCurrent?.obj_diapositiva.active_preguntas, context, handleClickOpenSesion)
          let r_questions_respuestas =  data[0]
          let r_questions = data[1]
          //console.log("r_questions_respuestas", r_questions_respuestas)
        }
        get_data()
      }
    },[modalStatus]);
  
    
    const [modulos, setModulos] = useState([]);
    const filterOption = (inputValue, option) => option.description.indexOf(inputValue) > -1;
    const handleChange = targetKeys => {
      setModulos({ ...modulos, targetKeys });
    };
    const handleSearch = (dir, value) => {
      console.log('search:', dir, value);
    };
    return (
        <>
          <Modal
              open={modalStatus}
              className={classes.modal}
              closeAfterTransition={false}
              BackdropComponent={Backdrop}
              BackdropProps={{
                  timeout: 0,
              }}
              onClose={abrirCerrarModalEditarCrear}>
                  <div className={classes.paperDiv}>
                    <Container component="main" className={classes.container}>
                      <CssBaseline />
                      <div className={classes.paper}>
                        <Grid container justifyContent="center">
                          <Grid item xs={12}>
                            <Typography component="h5" variant="h5">Ver</Typography>
                          </Grid>
                          <Grid item xs={12}>
                            <Typography component="h6" variant="h6">{editCurrent?.nombre}</Typography>
                          </Grid>
                          <Grid container justifyContent="center">
                              <iframe src={editCurrent?.obj_diapositiva.iframe} allow="autoplay" title="iframe Example 1" width="100%" height="500">
                                <p>Your browser does not support iframes.</p>
                              </iframe>
                           </Grid>
                           <Grid container >
                              <Button
                                  onClick={handleSubmit}
                                  type="submit"
                                  fullWidth
                                  variant="contained"
                                  color="primary"
                                  className={classes.submit}
                              >
                                Guardar
                              </Button>
                           </Grid>
                           <Grid container>
                            {/*
                              <Button variant="outlined" href="#outlined-buttons">
                                Agregar Modulos
                              </Button>
                            */}
                           </Grid>
                        </Grid>
                      </div>
                    </Container>
                    <DialogMsg open={openSesion} handleClose={handleCloseSesion} ttitle="Importante" msg="Sesión caducada"/>
                  </div>
          </Modal>
        </>
    )
}
export default View