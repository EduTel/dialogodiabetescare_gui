import React, { useEffect, useContext, useState } from 'react';
import userContext from "./../../../context/userContext"
//edit
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import RemoveIcon from '@material-ui/icons/Remove';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import Paper from '@material-ui/core/Paper';
import axios from 'axios';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import { Height } from '@material-ui/icons';

import asyncLocalStorage from '../../../utils/asyncLocalStorage'
import DialogMsg from "./../dialogMsg"
const { forwardRef, useRef, useImperativeHandle } = React;


const useStyles = makeStyles((theme) => ({
  paperDiv: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '80%',
    height: '80%',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  container: {
    width: '100%',
    height: '100%',
    padding: 0,
    overflow: 'scroll'
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
  },
  paper: {
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
      height: '100%',
      overflow: 'scroll'
  },
}));
function EditarCrearCuestionario(props) {
    const {preguntas, addPreguntas, deletePreguntas, handleInputChangeCuestionario, errorsFormCuestionario, valuesCuestionario} = props;
    const classes = useStyles();
    return (
      <div className={classes.paper}>
          <Typography component="h5" variant="h5">Cuestionario</Typography>
          <IconButton onClick={() => { addPreguntas() }}>
              <AddCircleOutlineIcon color="primary" />
          </IconButton>
          <Paper >
          {
            preguntas?.map(function(currElement, index1, array) { 
              console.log("*********************preguntas", index1+1)  
              let data =(<div key={currElement.id}>
                  <Grid container spacing={0}>
                    <Grid item xs={11}>
                      <TextField
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        name={currElement.id}
                        label={"pregunta: "+(parseInt(index1)+1)}
                        type="text"
                        id={currElement.id}
                        onBlur={handleInputChangeCuestionario}
                        onChange={handleInputChangeCuestionario}
                        value={valuesCuestionario[currElement.id]?.value}
                        required
                        {...(errorsFormCuestionario[currElement.id]?.value && {
                        error: true,
                        helperText: errorsFormCuestionario[currElement.id].value
                        })}
                      />
                    </Grid>
                    <Grid item xs={1}>
                      <IconButton onClick={(e) => { deletePreguntas(e, currElement.id) }}>
                          <RemoveIcon color="secondary" />
                      </IconButton>
                    </Grid>
                  </Grid>
                  <RadioGroup name={"rgg_"+currElement.id} onChange={handleInputChangeCuestionario} defaultValue={valuesCuestionario[currElement.id]?.defaultValue.toString()}>
                    {
                      currElement?.respuestas.map(function(currElementRespuestas, index2) {
                        console.log("respuestas", index2)
                        return (
                          <Grid container spacing={1} key={"p_"+currElement.id+"_fc_"+index2} >
                            <Grid item xs={2}>
                              <FormControlLabel value={(parseInt(index2)).toString()} control={<Radio color="primary" />} />
                            </Grid>
                            <Grid item xs={10}>
                              <TextField
                                variant="outlined"
                                margin="normal"
                                fullWidth
                                name={"fc_"+index1+"_"+index2}
                                label={"respuesta "+(parseInt(index2)+1)}
                                type="text"
                                id={"fc_"+index1+"_"+index2}
                                onBlur={(event) => handleInputChangeCuestionario(event, currElement.id ,parseInt(index2) )}
                                onChange={(event) => handleInputChangeCuestionario(event, currElement.id ,parseInt(index2) )}
                                value={valuesCuestionario[currElement.id]?.["respuestas"][index2]?.value}
                                required
                                {...(errorsFormCuestionario[currElement.id]?.respuestas?.[index2] && {
                                  error: true,
                                  helperText: errorsFormCuestionario[currElement.id]?.respuestas?.[index2]
                                })}
                              />
                            </Grid>
                          </Grid>
                        )
                      })
                    }
                  </RadioGroup>
                </div>)
              return data
            })
          }
          </Paper>
       </div>
    )
}
export default EditarCrearCuestionario