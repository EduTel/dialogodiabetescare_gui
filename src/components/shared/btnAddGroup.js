import React, { useEffect, useContext, useState } from 'react';
import userContext from "../../context/userContext"
import {IconButton} from '@material-ui/core';
import {GroupAdd} from  '@material-ui/icons';

function BtnAddGroup(props) {
    let { row, handleClickOpenModal, setRowSelected} = props
 
    function action(index){
        console.log("row2", index)
        setRowSelected(index)
        handleClickOpenModal()
    }

    return (
        <>
            <IconButton onClick={() => { action(row); }}>
                <GroupAdd color="primary" />
            </IconButton>
        </>
    )
}
export default BtnAddGroup