import React, { useState, useEffect, useMemo, useContext }  from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import userContext from "./../context/userContext"
import { useHistory } from "react-router-dom";
import Copyright from './legal/copyright'

/*
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
*/

import DialogMsg from './shared/dialogMsg'

const axios = require('axios');

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignUp() {
  const [context, setContext] = useContext(userContext);
  let history = useHistory();
  const initialFormValues = {
    first_name: "",
    last_name: "",
    especialidad: "",
    ciudad: "",
    estado: "",
    direccion: "",
    telefono: "",
    celular: "",
    email: "",
    password: "",
    password2: "",
    cedulaProfesional: "",
    legal: "",
  }
  const [values, setValues] = useState(initialFormValues);
  const [errorsForm, setErrorsForm] = useState({});
  const [errors, setErrors] = useState(false);
  const [errorsMsg, setErrorsMsg] = useState("");
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();
  
  const validate = (fieldValues = values) => {
    let temp = { ...errorsForm };

    if ("first_name" in fieldValues)
      temp.first_name = fieldValues.first_name ? "" : "El campo es requerido.";

    if ("last_name" in fieldValues)
      temp.last_name = fieldValues.last_name ? "" : "El campo es requerido.";

    if ("especialidad" in fieldValues)
      temp.especialidad = fieldValues.especialidad ? "" : "El campo es requerido.";

    if ("ciudad" in fieldValues)
      temp.ciudad = fieldValues.ciudad ? "" : "El campo es requerido.";

    if ("estado" in fieldValues)
      temp.estado = fieldValues.estado ? "" : "El campo es requerido.";

    if ("direccion" in fieldValues)
      temp.direccion = fieldValues.direccion ? "" : "El campo es requerido.";

    if ("telefono" in fieldValues)
      temp.telefono = fieldValues.telefono ? "" : "El campo es requerido.";

    if ("celular" in fieldValues)
      temp.celular = fieldValues.celular ? "" : "El campo es requerido.";

    if ("email" in fieldValues){
      temp.email = fieldValues.email ? "" : "El campo es requerido.";
      if (fieldValues.first_name)
        temp.email = /^[^@\s]+@[^@\s]+\.[^@\s]+$/.test(fieldValues.email)?"": "El Correo no es valido.";
    }

    if ("password" in fieldValues)
      temp.password = fieldValues.password ? "" : "El campo es requerido.";

    if ("password2" in fieldValues){
      temp.password2 = fieldValues.password2 ? "" : "El campo es requerido.";
      temp.password2 = values.password1!=fieldValues.password2 ? "" : "Las Contraseñas no son iguales.";
    }

    if ("cedulaProfesional" in fieldValues)
      temp.cedulaProfesional = fieldValues.cedulaProfesional ? "" : "El campo es requerido.";

    if ("legal" in fieldValues)
      temp.legal = fieldValues.legal ? "" : true;

    console.log("temp", temp)
    let enviar = true
    for(let row_data in temp){
      if(temp[row_data] != ""){
        enviar = false
        break
      }
    }
    setErrorsForm({
      ...temp
    });
    return enviar
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    history.push("/")
  };

  const handleSubmit = async (event) => {    
    event.preventDefault();
    console.log("values:")
    console.log(values)
    //for (let name in values){
    //  console.log({ [name]: initialFormValues[name]})
    //  validate({ [name]: initialFormValues[name]});
    //}
    let enviar = validate(values);
    console.log("enviar", enviar)
    if( enviar )
      axios.post(`http://localhost:8000/cursos/api/registro`,values)
      .then(res => {
          console.log("handleClickOpen")
          console.log(res.data)
          handleClickOpen()
          //let data = res.data;
          //console.log(data?.token)
          //if (data?.token != undefined){
          //  console.log(data)
          //  setContext(data)
          //  history.push("/Home")
          //}else{
          //  setErrors(true)
          //  setErrorsMsg("Usuario o contraseña incorrecto")
          //}
          //setDatos([
          //    ...datos,
          //    data
          //])
          //setFormulario(default_formulario)
          //alert("Datos guardados")
      }).catch(function (error) {
        console.log(error.response);
        console.log("error.request.responseText", typeof error.request.responseText, error.request.responseText);
        console.log(error.message);
        let temp = { ...errorsForm };
        let response400json = JSON.parse(error.request.responseText)
        console.log(response400json, typeof response400json, Object.getOwnPropertyNames(response400json), typeof Object.getOwnPropertyNames(response400json))
        if (Object.getOwnPropertyNames(response400json).includes("password")){
          //console.log(response400json.get["password"])
          temp.password = ""
          for( let row_data of response400json?.password){
            console.log(row_data)
            if(row_data == "This password is too short. It must contain at least 8 characters.")
              temp.password = "La contraseña es demasiado corta. debe contener al menos 8 caracteres";
            if(row_data == "This password is too common.")
              temp.password = "La contraseña es demasiado sencilla y comun"
            if(row_data == "This password is entirely numeric.")
              temp.password = "La contraseña solo contiene numeros";
          }
        }
        if (Object.getOwnPropertyNames(response400json).includes("email")){
          //console.log(response400json.get["email"])
          temp.email = ""
          for( let row_data of response400json?.email){
            console.log(row_data)
            if(row_data == "Enter a valid email address.")
              temp.email = "Ponga un email valido";
          }
        }
        setErrorsForm({
          ...temp
        });
      });
      //setContext({"name": formulario.username.trim(), acount: 123456789})
      //
}
  const handleInputChange = (e) => {
    const { name, value, checked } = e.target;
    console.log("------->", name, value, checked)
    if(name == "legal"){
      setValues({
        ...values,
        [name]: checked
      });
      validate({ [name]: checked });
    }else{
      setValues({
        ...values,
        [name]: value
      });
      validate({ [name]: value });
    }
  };
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Crea cuenta
        </Typography>
        <form className={classes.form} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="fname"
                name="first_name"
                variant="outlined"
                required
                fullWidth
                id="first_name"
                label="Nombre"
                onBlur={handleInputChange}
                onChange={handleInputChange}
                {...(errorsForm["first_name"] && {
                  error: true,
                  helperText: errorsForm["first_name"]
                })}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="last_name"
                label="Apellidos"
                name="last_name"
                autoComplete="last_name"
                onBlur={handleInputChange}
                onChange={handleInputChange}
                {...(errorsForm["last_name"] && {
                  error: true,
                  helperText: errorsForm["last_name"]
                })}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="especialidad"
                label="Especialidad"
                name="especialidad"
                autoComplete="especialidad"
                onBlur={handleInputChange}
                onChange={handleInputChange}
                {...(errorsForm["especialidad"] && {
                  error: true,
                  helperText: errorsForm["especialidad"]
                })}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="fname"
                name="ciudad"
                variant="outlined"
                required
                fullWidth
                id="ciudad"
                label="Ciudad"
                onBlur={handleInputChange}
                onChange={handleInputChange}
                {...(errorsForm["ciudad"] && {
                  error: true,
                  helperText: errorsForm["ciudad"]
                })}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="estado"
                label="Estado"
                name="estado"
                autoComplete="estado"
                onBlur={handleInputChange}
                onChange={handleInputChange}
                {...(errorsForm["estado"] && {
                  error: true,
                  helperText: errorsForm["estado"]
                })}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="direccion"
                label="Dirección"
                type="direccion"
                id="direccion"
                autoComplete="direccion"
                onBlur={handleInputChange}
                onChange={handleInputChange}
                {...(errorsForm["direccion"] && {
                  error: true,
                  helperText: errorsForm["direccion"]
                })}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="fname"
                name="telefono"
                variant="outlined"
                required
                fullWidth
                id="telefono"
                label="Teléfono"
                onBlur={handleInputChange}
                onChange={handleInputChange}
                {...(errorsForm["telefono"] && {
                  error: true,
                  helperText: errorsForm["telefono"]
                })}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="celular"
                label="Celular"
                name="celular"
                autoComplete="celular"
                onBlur={handleInputChange}
                onChange={handleInputChange}
                {...(errorsForm["celular"] && {
                  error: true,
                  helperText: errorsForm["celular"]
                })}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="email"
                label="Correo electrónico"
                type="email"
                id="email"
                autoComplete="email"
                onBlur={handleInputChange}
                onChange={handleInputChange}
                {...(errorsForm["email"] && {
                  error: true,
                  helperText: errorsForm["email"]
                })}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Contraseña"
                type="password"
                id="password"
                autoComplete="password"
                onBlur={handleInputChange}
                onChange={handleInputChange}
                {...(errorsForm["password"] && {
                  error: true,
                  helperText: errorsForm["password"]
                })}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password2"
                label="Confirmar Contraseña"
                type="password"
                id="password2"
                autoComplete="password2"
                onBlur={handleInputChange}
                onChange={handleInputChange}
                {...(errorsForm["password2"] && {
                  error: true,
                  helperText: errorsForm["password2"]
                })}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="cedulaProfesional"
                label="Cédula profesional"
                type="cedulaProfesional"
                id="cedulaProfesional"
                autoComplete="cedulaProfesional"
                onBlur={handleInputChange}
                onChange={handleInputChange}
                {...(errorsForm["cedulaProfesional"] && {
                  error: true,
                  helperText: errorsForm["cedulaProfesional"]
                })}
              />
            </Grid>
            <Grid item xs={12}>
              {/*<Checkbox value="allowExtraEmails" color="primary" required/>*/}
              <FormControlLabel control={<Checkbox onChange={handleInputChange} name="legal" required />}
              />
              <Typography className={classes.root} display="inline"                   {
                    ...(errorsForm["legal"]==true && {
                        style: {
                          color: "red"
                        }
                      })
                  }>
                He leído y acepto el 
                <Link href="/privacidad"> Aviso de Privacidad </Link>
                , la 
                <Link href="/legal"> Declaración Legal </Link>
                y la 
                <Link href="/cookies"> Política sobre cookies </Link>
              </Typography>
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={handleSubmit}
          >
            Crea cuenta
          </Button>
          <Grid container justifyContent="flex-end">
            <Grid item>
              <Link href="/">
                ¿Ya tienes una cuenta? entrar
              </Link>
            </Grid>
          </Grid>
          <DialogMsg open={open} handleClose={handleClose} ttitle="Importante" msg="Favor de revisar su correo electronico."/>
          {/*
          <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">{"Importante"}</DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-dialog-description">
                  Favor de revisar su correo electronico.
                </DialogContentText>
              </DialogContent>
            <DialogActions>
              <Button onClick={handleClose} color="primary" autoFocus>
                Cerrar
              </Button>
            </DialogActions>
          </Dialog>
            */}
        </form>
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  );
}