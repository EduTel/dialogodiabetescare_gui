import React, { useEffect, useContext, useState } from 'react';
import userContext from "../context/userContext"
import './../static/css/template.css';

import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';

const useStyles = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(2, 2, 2, 2),
    }
}));

function OlvideMiPassword(props) {
    const [context, setContext] = useContext(userContext);
    const classes = useStyles();

    return (
        <>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <div className={classes.paper}>
                    <Typography component="h1" variant="h5">
                        Recuperar contraseña
                    </Typography>
                    <form className={classes.form} noValidate>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Ingresa tu correo electrónico"
                            name="email"
                            autoComplete="email"
                            autoFocus
                        />
                        <Grid item xs={12}>
                            <Grid container justifyContent="center" spacing={3}>
                                <Grid item xs={12} sm={6}> 
                                    <Button
                                        href="/"
                                        type="submit"
                                        fullWidth
                                        variant="contained"
                                        color="secondary"
                                    >
                                        Cancelar
                                    </Button>
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <Button
                                        type="submit"
                                        fullWidth
                                        variant="contained"
                                        color="primary"
                                    >
                                        Entrar
                                    </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Container>
        </>
    )
}
export default OlvideMiPassword