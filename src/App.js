import logo from './logo.svg';
import './App.css';
import React, { useState, useEffect } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import userContext from "./context/userContext"
import Header from './components/shared/header'
import { Container } from '@material-ui/core';

function App() {
  const [context, setContext] = useState();
  return (
    <userContext.Provider value={[context, setContext]}>
      <Header/>
    </userContext.Provider>
  );
}

export default App;
